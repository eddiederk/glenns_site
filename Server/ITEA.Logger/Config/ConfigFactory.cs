﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ITEA.Logger.Listeners;
using System.IO;
using System.ComponentModel;

namespace ITEA.Logger.Config
{
	internal static class ConfigFactory
	{
		public const string VERSION = "2";
		private const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;


		public static Dictionary<string, string> GetSettings(this ListenerSettings settings)
		{
			var t = settings.GetType();
			var props = t.GetProperties(BindingFlags.Instance | BindingFlags.SetProperty | BindingFlags.Public);
			var results = new Dictionary<string, string>();
			foreach (var p in props)
			{
				string name = p.Name;
				object value = p.GetValue(settings);
				if (value != null)
				{
					if (p.IsDefined(typeof(DescriptionAttribute), true))
					{ // See we should use a friendly name using DescriptionAttribute
						var a = (DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), true);
						if (a.Length > 0) name = a[0].Description;
					}

					results.Add(name, value.ToString());
				}
			}
			return results;
		}

		public static PropertyInfo GetProperty(this ListenerSettings settings, string propertyName)
		{
			var t = settings.GetType();
			var props = t.GetProperties(BindingFlags.Instance | BindingFlags.SetProperty | BindingFlags.Public);
			foreach (var p in props)
			{
				if (p.Name == propertyName) return p;
				else if (p.IsDefined(typeof(DescriptionAttribute), true))
				{ // See we should use a friendly name using DescriptionAttribute
					var a = (DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), true);
					if (a[0].Description == propertyName) return p;
				}
			}

			return null;
		}

		public static void WriteXMLElements(this ListenerSettings settings, XmlWriter w)
		{
			var values = settings.GetSettings();
			foreach (var p in values)
			{
				if (p.Key == "Tracing" && p.Value == ListenerSettings.DEFAULT_TRACE_LEVEL.ToString()) continue;
				if (p.Key == "DateTimeFormat" && (string)p.Value == ListenerSettings.DEFAULT_DATE_FORMAT) continue;
				if (p.Key == "Options" && p.Value == ListenerSettings.DEFAULT_OPTION.ToString()) continue;
				w.WriteAttributeString(p.Key, p.Value);
			}
		}

		public static ListenerSettings CreateSettings(Type t, XmlElement xml)
		{
			ConstructorInfo ctor = t.GetConstructor(flags, null, null, null);
			if (ctor == null)
				throw new ApplicationException("Couldn't find constructor for class " + t);

			var settings = (ListenerSettings)ctor.Invoke(null);
			var properties = t.GetProperties(flags | BindingFlags.SetProperty);
			foreach (XmlNode node in xml.ChildNodes)
			{
				var p = properties.FirstOrDefault(x => x.Name == node.Name);
				if (p != null)
				{
					p.SetValue(settings, node.Value);
				}
			}

			return settings;
		}

		public static ILogListener LoadListener(this Logger log, XmlReader r)
		{
			var typeAttribute = r.GetAttribute("Type")??r.GetAttribute("type");
			var nameAttribute = r.GetAttribute("Name")??r.GetAttribute("name");

			// Find Existing Listener
			var listener = log.Listeners[nameAttribute];

			if (listener == null)
			{	// Create Listener
				try
				{
					Type t = Type.GetType(typeAttribute, false, true);
					var tts = t.GetNestedTypes();
					ConstructorInfo ctor = t.GetConstructor(Type.EmptyTypes);
					if (ctor == null) throw new ApplicationException("Couldn't find constructor for class " + t);

					listener = (ILogListener)ctor.Invoke(null);
					listener.Name = nameAttribute;
				}
				catch(Exception ex)
				{
					string error = Log.GetExceptionMessage(ex, false);
					Console.WriteLine("Failed to create Listener: " + typeAttribute + " error:" + error);
				}
			}
			
			//Load Settings
			if (listener!= null && listener.Settings != null)
			{
				var settingsType = listener.Settings.GetType();
				var properties = settingsType.GetProperties(flags | BindingFlags.SetProperty);
				foreach (var p in properties)
				{ //See if the property exists as an attribute in the xml file
					string value = r.GetAttribute(p.Name);
					if (value == null && p.IsDefined(typeof(DescriptionAttribute),true))
					{ // See if the xml attribute is using a different name as Defined by the Propertys DescriptionAttribute
						var a =  (DescriptionAttribute[])p.GetCustomAttributes(typeof(DescriptionAttribute), true);
						if (a.Length > 0) value = r.GetAttribute(a[0].Description);
					}
					if (value != null)
					{
						TypeConverter tc = TypeDescriptor.GetConverter(p.PropertyType);
						try
						{
							var oValue = tc.ConvertFromString(value);
							p.SetValue(listener.Settings, oValue);
						}
						catch (Exception ex)
						{
							Console.WriteLine(String.Format("Failed to set Listener Settings {0}.{1} to {2}", listener.Name, p.Name, value));
							Log.Write(MessageType.Error, ex, "Failed to set Listener Settings {0}.{1} to {2}", listener.Name, p.Name, value );
						}
					}
				}
			}

			return listener;
		}

		public static ILogListener[] UpgradeFromOldConfig(this Logger log, string version, XmlReader r)
		{
			switch (version)
			{
				case "1": return UpgradeFromV1(log, r);
			}
			return null;
		}

		private static ILogListener[] UpgradeFromV1(Logger log, XmlReader r)
		{
			List<ILogListener> list = new List<ILogListener>();

			while (r.Read())
			{
				if (r.NodeType != XmlNodeType.Element) continue;

				if (r.Name == "ConsoleTracing")
				{
					var l = log.Listeners["Console"];
					if (l == null) l = new ConsoleListener();
					l.Settings.TraceLevel = long.Parse(r.ReadInnerXml().Trim('"'));
					list.Add(l);
				}
				else if (r.Name == "LogFileTracing")
				{
					var l = log.Listeners["File"];
					if (l == null) l = new FileListener();
					l.Settings.TraceLevel = long.Parse(r.ReadInnerXml().Trim('"'));
					list.Add(l);
				}
				else if (r.Name == "PipeTracing")
				{
					var l = log.Listeners["Pipe"];
					if (l == null) l = new Pipes.PipeListener();
					l.Settings.TraceLevel = long.Parse(r.ReadInnerXml().Trim('"'));
					list.Add(l);
				}
				else if (r.Name == "HoursToHoldLogFiles")
				{
					var l = list.FirstOrDefault(x => x.Name == "File");
					if (l == null) l = new FileListener();
					((FileListener)l).Settings.HoursToKeepLogFiles = int.Parse(r.ReadInnerXml().Trim('"'));
				}
			}

			return list.ToArray();
		}
	}
}
