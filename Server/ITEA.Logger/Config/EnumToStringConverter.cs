﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITEA.Logger.Config
{
	/// <summary>
	/// Converts an Enum to a String w/ an optional use of the Description attribute
	/// </summary>
	internal class EnumToStringConverter : EnumConverter
	{
		public EnumToStringConverter(Type type) : base(type)
		{
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || TypeDescriptor.GetConverter(typeof(Enum)).CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string) return GetEnumValue(EnumType, (string)value);
			if (value is Enum) return GetEnumDescription((Enum)value);
			return base.ConvertFrom(context, culture, value);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if(value is Enum && destinationType == typeof(string)) return GetEnumDescription((Enum)value);
			else if(value is string && destinationType == typeof(string)) return GetEnumDescription(EnumType, (string)value);
			else return base.ConvertTo(context, culture, value, destinationType);
		}

		public static string GetEnumDescription(Enum value, bool checkFlagsAttribute = true)
		{
			if (checkFlagsAttribute && value.GetType().IsDefined(typeof(FlagsAttribute), inherit: false))
			{
				string result = "";
				foreach (Enum x in Enum.GetValues(value.GetType()))
				{
					if (value.HasFlag(x)) 
					result += "|" + GetEnumDescription(x, false);
				}

				return result.TrimStart('|');
			} 


			var fieldInfo = value.GetType().GetField(value.ToString());
			var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
			return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
		}

		public static string GetEnumDescription(Type value, string name)
		{
			var fieldInfo = value.GetField(name);
			var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
			return (attributes.Length > 0) ? attributes[0].Description : name;
		}

		public static object GetEnumValue(Type value, string description, bool checkFlagsAttribute = true)
		{
			if (checkFlagsAttribute && value.IsDefined(typeof(FlagsAttribute), inherit: false))
			{
				int result = 0;
				foreach (var d in description.Split('|'))
				{
					object o = GetEnumValue(value, d, false);
					if (o.GetType() == value) result += (int)o;
				}
				
				return result;
			} 

			var fields = value.GetFields();
			foreach (var fieldInfo in fields)
			{
				var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
				if (attributes.Length > 0 && attributes[0].Description == description)
					return fieldInfo.GetValue(fieldInfo.Name);
				if (fieldInfo.Name == description)
					return fieldInfo.GetValue(fieldInfo.Name);
			}
			
			return description;
		}
	}
}
