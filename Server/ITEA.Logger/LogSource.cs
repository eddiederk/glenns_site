﻿using ITEA.Logger.Listeners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITEA.Logger
{
	public interface ILogSource
	{
		string SourceName { get; }
	}

	public static class LogSourceFactory
	{
		public static void LogMessage(this ILogSource source, MessageType messageType, string strMessage, params object[] args) 
		{
			Log.WriteMessageArgs<int>(source.SourceName, messageType, null, strMessage, args);
		}
		
		public static void LogMessage(this ILogSource source, MessageType messageType, Exception ex, string strMessage, params object[] args)
		{
			Log.WriteMessageArgs<int>(source.SourceName, messageType, null, strMessage, args);
		}

		public static void LogMessageTo<T>(this T source, MessageType messageType, string strMessage, params object[] args)
			where T : ILogListener 
		{
			Log.WriteMessageArgs<T>(source.Name, messageType, null, strMessage, args);
		}
	}
}
