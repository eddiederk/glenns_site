﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.IO.Pipes;
using ITEA.Logger.Listeners;
using System.Collections.Concurrent;
using System.Security.Principal;
using System.Security.AccessControl;

namespace ITEA.Logger.Pipes
{
	public delegate void NewLogPipeMessageHandler(string strCommand, MessageType type);
	public delegate string CmdReceivedHandler(string strCommand);

	public class Pipe :IDisposable
	{
		#region Properties/Fields
		//writeLine updates m_Queue while a non-blocking thread reads from it.
		private BlockingCollection<string> m_Queue = new BlockingCollection<string>();
		
		//public event NewLogPipeMessageHandler LogMessageNew;
		public event CmdReceivedHandler CmdReceived;

		private NamedPipeServerStream _pipeOutput;
		private StreamWriter _pipeWriter;
		private NamedPipeServerStream _pipeInput;
		private StreamReader _pipeReader;
		private static int _maxId = 1;
		private Thread _nonBlockingWriteThread;
		private string _pipeName;
		private PipeState _state;
		private bool _isDisposed = false;
		private IAsyncResult _pipeOutputWaiting;
		// private PipeSecurity pipeSecurityAccess;
		#endregion

		#region Constructors

		public Pipe(int ID)
		{
			this.ID = ID;
			_state = PipeState.New;
			// pipeSecurityAccess = new PipeSecurity();
			// pipeSecurityAccess.SetAccessRule(new PipeAccessRule(new SecurityIdentifier(WellKnownSidType.AuthenticatedUserSid, null), PipeAccessRights.ReadData, AccessControlType.Allow));
			
		}
		public Pipe() : this("noname") { }
		public Pipe(string Name) : this(_maxId++) 
		{ 
			_pipeName = Name;
		}

		~Pipe()
		{
			_pipeWriter = null;
			_pipeReader = null;
			_pipeOutput = null;
		}
		#endregion

		#region Properties

		public int ID { get; set; }
		public PipeState State { get { return _state; } }

		#endregion

		public void Dispose()
		{
			_isDisposed = true;
			try {
				if(_pipeOutputWaiting != null)
				{
					_pipeOutputWaiting.AsyncWaitHandle.Close();
					_pipeOutput.EndWaitForConnection(_pipeOutputWaiting);
				}
				if (_pipeOutput != null) _pipeOutput.Close(); 
			}
			catch (Exception ex) { 
			
			}
			try { if (_pipeInput != null) _pipeInput.Close(); } catch (Exception) { }

			_pipeInput = null;
			_pipeOutput = null;
			//Remove Event Handlers...
			CmdReceived = null;
			//base.Dispose();
			
		}
		public void Close()
		{
			_state = PipeState.Closed;

			if (_pipeOutput != null)
			{
				if (_pipeOutput.IsConnected)
					_pipeOutput.Close();

			}
			if (_pipeInput != null)
			{
				if (_pipeInput.IsConnected)
					_pipeInput.Close();
			}

			try { this._nonBlockingWriteThread.Abort(); }
			catch (Exception) { }
		}
		
		public void Start()
		{
			if (_isDisposed) return;
			try
			{
				_state = PipeState.Waiting;

				_pipeOutput = new NamedPipeServerStream(_pipeName, PipeDirection.Out, 5, PipeTransmissionMode.Message, PipeOptions.Asynchronous);
				_pipeOutput.ReadMode = PipeTransmissionMode.Message;
				// _pipeOutput.SetAccessControl(pipeSecurityAccess);
				
				//LogMyMessage("New Pipe waiting for connection ID:" + _id.ToString(), MessageType.Pipe);
				Log.Write(PipeMessageType.Pipe, "New Pipe waiting for connection ID:{0}", ID.ToString());
				_pipeOutputWaiting = _pipeOutput.BeginWaitForConnection((IAsyncResult r) => {
					//LogMyMessage("Received Connection on Pipe ID:" + _id.ToString(), MessageType.Pipe);
					_pipeOutput.EndWaitForConnection(r);
					_pipeOutputWaiting = null;
					_state = PipeState.Configuring;
					Log.Write(PipeMessageType.Pipe, "Received Connection on Pipe ID:{0}", ID.ToString());
					_pipeWriter = new StreamWriter(_pipeOutput);
					_pipeWriter.AutoFlush = true;

					if (_pipeOutput.IsConnected)
					{
						Thread pipeInputThread = new Thread(InputThread);    //TODO: no need for a new thread
						pipeInputThread.Start();

						this._nonBlockingWriteThread = new Thread(this.NonBlockingWrite);
						this._nonBlockingWriteThread.IsBackground = true;
						this._nonBlockingWriteThread.Start();
					}
				}, null);    // Blocking method.
			}
			catch (IOException ex)
			{
				_state = PipeState.Closed;
				//LogMyMessage("Pipe ID:" + _id.ToString() + " ERROR: " + ex.Message, MessageType.Pipe);
				Log.Write(PipeMessageType.Pipe, "Pipe ID:{0} ERROR: {1}", ID.ToString(), ex.Message);
			}
			catch (UnauthorizedAccessException ex)
			{
				_state = PipeState.Closed;
				Log.Write(PipeMessageType.Pipe, "Pipe ID:{0} ERROR: {1}", ID.ToString(), ex.Message);
			}
		}

		public void WriteLine(string Message)
		{
			m_Queue.Add(Message);
			//try
			//{
			//	if (_state == PipeState.OutputMode || _state == PipeState.CommandMode)
			//	{
			//		if (_pipeOutput.IsConnected)
			//			_pipeWriter.WriteLine(Message);
			//	}
			//}
			//catch (IOException ex)
			//{
			//	_state = PipeState.Closed;
			//	_pipeOutput.Close();
			//	Log.Write( PipeMessageType.Pipe, "Error on Pipe ID:{0} Message: {1}",ID.ToString(), ex.Message);
			//	Log.Write(PipeMessageType.Pipe, "Closing Pipe ID:{0}", ID.ToString());
			//	//LogMyMessage("Error on Pipe ID:" + _id.ToString() + " Message:" + ex.Message, MessageType.Pipe);
			//	//LogMyMessage("Closing Pipe ID:" + _id.ToString(), MessageType.Pipe);
			//}
		}

		private void NonBlockingWrite() 
		{
			while (!_isDisposed)
			{
				try
				{
					if (_state == PipeState.OutputMode || _state == PipeState.CommandMode)
					{
						if (_pipeOutput.IsConnected)
							_pipeWriter.WriteLine(m_Queue.Take());
					}
				}
				catch (ObjectDisposedException)
				{
					return;
				}
				catch (ThreadAbortException)
				{
					Log.Write(PipeMessageType.Pipe, "Aborting the writer thread ID:{0}", ID.ToString());
					return;
				}
				catch (IOException ex)
				{
					_state = PipeState.Closed;
					_pipeOutput.Close();
					Log.Write(PipeMessageType.Pipe, "Error on Pipe ID:{0} Message: {1}", ID.ToString(), ex.Message);
					Log.Write(PipeMessageType.Pipe, "Closing Pipe ID:{0}", ID.ToString());
					//LogMyMessage("Error on Pipe ID:" + _id.ToString() + " Message:" + ex.Message, MessageType.Pipe);
					//LogMyMessage("Closing Pipe ID:" + _id.ToString(), MessageType.Pipe);
				}
			}
		}

		private void InputThread()
		{
			try
			{
				// Setup InputPipe - Send my PipeID to client to negotiate an inbound pipe.
				_pipeWriter.WriteLine(ID.ToString());
				_pipeInput = new NamedPipeServerStream(_pipeName + ID.ToString(), PipeDirection.In, 1, PipeTransmissionMode.Message, PipeOptions.Asynchronous);

				//LogMyMessage("Waiting for inbound pipe connection from client. ID:" + _id.ToString(), MessageType.Pipe);
				Log.Write(PipeMessageType.Pipe, "Waiting for inbound pipe connection from client. ID:{0}", ID.ToString());
				_pipeInput.WaitForConnection();    // Blocking method.
				//LogMyMessage("Received Connection on Pipe ID:" + _id.ToString(), MessageType.Pipe);
				Log.Write(PipeMessageType.Pipe, "Received Connection on Pipe ID: {0}", ID.ToString());
				_pipeReader = new StreamReader(_pipeInput);
				_state = PipeState.CommandMode;

				string input, response;
				while (_pipeInput != null && _pipeInput.IsConnected)
				{
					if ((input = _pipeReader.ReadLine()) != null)
					{
						_state = PipeState.CommandMode;
						if (CmdReceived != null)
						{
							//LogMyMessage("Recieved message on Pipe ID:" + _id.ToString() + " Message:'" + input + "'", MessageType.Pipe);
							Log.Write(PipeMessageType.Pipe, "Recieved message on Pipe ID:{0} Message: '{1}'", ID.ToString(), input);

							response = "";
							if (input.Trim().ToLower() == "stop")
							{
								_state = PipeState.CommandMode;
							}
							else if (input.Trim().ToLower() == "start")
							{
								_state = PipeState.OutputMode;
							}
							else
							{
								response = CmdReceived(input.Trim()); // lowercase everything and trim spaces
								WriteLine(response);
							}
						}
					}

					Thread.Sleep(11);
				}
			}
			catch (IOException ex)
			{
				//LogMyMessage("ERROR: " + ex.Message, MessageType.Pipe);
				Log.Write(PipeMessageType.Pipe, "ERROR: {0}", ex.Message);
			}

		}

		//private void LogMyMessage(string message, MessageType type)
		//{
		//	if (LogMessageNew != null)  LogMessageNew(message, type);
		//}

		public override bool Equals(object obj)
		{
			//FIX: Type check 
			if (obj.GetType() != typeof(Pipe)) return false;

			Pipe o = (Pipe)obj;

			if (o.ID == this.ID) return true;
			else return false;
		}
		
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

	}

}
