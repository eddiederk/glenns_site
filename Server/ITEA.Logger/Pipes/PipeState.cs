﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITEA.Logger
{
	public enum PipeState
	{
		New = 0,
		Waiting = 1,
		Configuring = 2,
		OutputMode = 3,
		CommandMode = 4,
		Closed = 5
	}
}
