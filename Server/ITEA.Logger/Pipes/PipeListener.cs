﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ITEA.Logger.Pipes
{
	public class PipeListener : Listeners.LogListener<ITEA.Logger.Listeners.ListenerSettings, PipeMessageType>
	{
		private ArrayList _pipes;               // <-- NEW
		private int _pipeCount;                 // <-- NEW
		private static int _maxPipeCount = 5;   // <-- NEW
		private string strName;
		private CmdReceivedHandler cmdHandler;
		


		public PipeListener()
		{
			Name = "Pipe";
			_pipes = new ArrayList();
		}

		public override void Initalize(Logger logger)
		{
			base.Initalize(logger);
			this.strName = logger.strMyName;
			
			//Start Server
			Thread serverThread = new Thread(() => { ManagePipes(logger); });
			serverThread.Start();

			Thread.Sleep(3000);  // wait for output pipes to set up     TODO: Set to 10 seconds for implementation.
		}

		private void ManagePipes(Logger logger)
		{
			//Wait until command manager is created.
			while (logger.CommandManager == null) Thread.Sleep(250); 
			//Create delegate.
			cmdHandler = logger.CommandManager.CommandReceived;
			
			ArrayList completedPipes;
			int waitingPipes;
			while (!IsDisposed)
			{
				completedPipes = new ArrayList();
				waitingPipes = 0;

				lock (_pipes)
				{
					foreach (Pipe cp in  _pipes)
					{
						switch (cp.State)
						{
							case PipeState.Closed:
								cp.Dispose();
								//LogMessage("Marking Pipe for removal. ID:" + cp.ID.ToString(), LogMessageType.Pipe);
								Log.Write(PipeMessageType.Pipe, "Marking Pipe for removal. ID:{0}", cp.ID.ToString());
								completedPipes.Add(cp);
								break;
							case PipeState.Waiting:
								waitingPipes++;

								if (waitingPipes > 1)    // Only need 1 pipe waiting.
								{
									//LogMessage("Too many pipes open.  Marking pipe for removal. ID:" + cp.ID.ToString(), LogMessageType.Pipe);
									Log.Write(PipeMessageType.Pipe, "Too many pipes open.  Marking pipe for removal. ID:{0}", cp.ID.ToString());
									cp.Dispose();
									completedPipes.Add(cp);
								}
								break;
							default:
								break;
						}
					}
					// remove Pipes that were marked for removal
					foreach (Pipe cp in completedPipes)
					{
						//LogMessage("Removing Pipe from memory ID:" + cp.ID.ToString(), LogMessageType.Pipe);
						Log.Write(PipeMessageType.Pipe, "Removing Pipe from memory ID:{0}", cp.ID.ToString());
						cp.Close();
						cp.Dispose();
						_pipes.Remove(cp); //_pipes.Remove(cp);
						_pipeCount--;
					}

					// Open another pipe if needed.  Keep 1 pipe waiting for connection at all times
					if (waitingPipes < 1 && _pipeCount < _maxPipeCount)
					{
						Pipe pipe = new Pipe(strName);
						
						//ConsolePipe pipe = new ConsolePipe(strMyName);
						pipe.CmdReceived += cmdHandler;
						//pipe.LogMessage += LogMessage;
						//pipe.LogMessageNew += Log.LogMessage;
						//LogMessage("Starting another open Pipe:" + pipe.ID.ToString(), LogMessageType.Pipe);
						Log.Write(PipeMessageType.Pipe, "Starting another open Pipe:{0}", pipe.ID.ToString());
						Thread serverThread = new Thread(pipe.Start);
						serverThread.Start();

						_pipes.Add(pipe);
						_pipeCount++;
					}
				}

				Thread.Sleep(1001);
			}
		}

		private void WaitForCommandManagerToGetCreated(Logger logger)
		{
			while(logger.CommandManager == null)
			{	// Wait until the command manager is created...
				Thread.Sleep(500);
			}
			this.cmdHandler = logger.CommandManager.CommandReceived;
		}

		[RegisterCommand("killpipes", "Kills all open Pipe connections from clients", "killpipes")]
		private string CmdKillAllPipes(string[] args)
		{
			lock (_pipes)
			{
				foreach (Pipes.Pipe cp in _pipes)
				{
					if (cp.State != PipeState.Closed)
					{
						Log.Write(PipeMessageType.Pipe, "Closing Pipe with ID:{0}", cp.ID.ToString());
						cp.WriteLine("BANG! You're dead");
						cp.Close();
						cp.Dispose();
					}
				}
			}

			return "";
		}
		
		[RegisterCommand("close","Closes the PipeClient application", "close [ClientID]")]
		private string CmdClosePipe(string[] args)
		{
			lock (_pipes)
			{
				foreach (Pipe cp in _pipes)
				{
					if (cp.ID == Convert.ToInt32(args[0]))
					{
						Log.Write(PipeMessageType.Pipe, "Closing Pipe with ID:{0}", cp.ID.ToString());
						cp.WriteLine("BANG! You're dead");
						cp.Close();
						cp.Dispose();
					}
				}

			}
			return "";
		}
		
		[RegisterCommand("PipeStatus", "Displays current status summary for all Named Pipes in use")]
		private string CmdPipeStatusSummary(string[] args)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("TODO: Implementation needed.");
			return sb.ToString();
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			CmdKillAllPipes(null);
		}

		public override void Write(LogData message)
		{
			string m = GetFormattedMessage(message);
			foreach (Pipe p in _pipes)
			{
				if(p.State == PipeState.OutputMode) p.WriteLine(m);
			}
		}

		public override void Flush() { }

		public override MessageType LogMessageType { get { return PipeMessageType.Pipe; } }
	}

	public class PipeMessageType : MessageType
	{
		public static readonly MessageType Pipe = Add(0x00000040, "|", "Pipe");
	}
}
