﻿using ITEA.Logger.Listeners;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ITEA.Logger
{
	public class CommandFactory
	{
		public static void RegisterAllCommands(Object objectToAutoRegister)
		{
			Log.CommandManager.AutoRegisterCommands(objectToAutoRegister);
		}

		public static void RegisterAllCommands()
		{
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			foreach (System.Reflection.Assembly b in assemblies)
			{
				var fullName = b.FullName.ToLower();
				if (!fullName.Contains("publickeytoken=null")) continue;
				foreach (var t in b.GetTypes())
				{
					Log.CommandManager.AutoRegisterCommands(t);
				}
			}
		}
	}

	namespace Command
	{
		internal class CommandManager : IDisposable
		{
			private ArrayList _commands = new ArrayList();            // <-- NEW

			public CommandManager()
			{
				// Following commands are handled by the PipeClient app or ConsolePipe class.
				_commands.Add(new ConsoleCommand(null, "stop", "Stop logging realtime traces to client", "stop (or any key, while traces are enabled"));
				_commands.Add(new ConsoleCommand(null, "start", "Start logging realtime traces to client", "start"));
				_commands.Add(new ConsoleCommand(null, "exit", "Exits the PipeClient application", "exit"));
				_commands.Add(new ConsoleCommand(null, "connect", "Establishes connection between PipeClient and the application", "connect"));

				// Commands handled by me (ConsoleLogger).
				_commands.Add(new ConsoleCommand(CmdHelp, "Help", "Display available commands"));
				_commands.Add(new ConsoleCommand(CmdHelp, "?", "Shortcut to display available commands"));
			}

			public void AutoRegisterCommands(Object objectToAutoRegister)
			{
				AutoRegisterCommands(objectToAutoRegister.GetType(), objectToAutoRegister);
			}

			internal void AutoRegisterCommands(Type type, Object o = null)
			{
				Log.Write(MessageType.Debugging, "Registering Commands for {0}...", type.Name);
				var bindingFlags = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public;
				if (o == null) bindingFlags |= System.Reflection.BindingFlags.Static;
				else bindingFlags |= System.Reflection.BindingFlags.Instance;

				var methods = type.GetMethods(bindingFlags);
				foreach (System.Reflection.MethodInfo mInfo in methods)
				{
					try
					{
					RegisterCommandAttribute attr = (RegisterCommandAttribute)Attribute.GetCustomAttribute(mInfo, typeof(RegisterCommandAttribute));
					if (attr == null) continue;

					if (string.IsNullOrEmpty(attr.CommandName)) attr.CommandName = mInfo.Name; // Use the method name as the default CommandName
					
						Delegate d = null;
						if (mInfo.IsStatic) d = Delegate.CreateDelegate(typeof(ConsoleCommandDelegate), type, mInfo.Name); //get the delegate that will be called
						else if(o!=null) d = Delegate.CreateDelegate(typeof(ConsoleCommandDelegate), o, mInfo.Name); //get the delegate that will be called
						

						if (string.IsNullOrEmpty(attr.UsageString)) _commands.Add(new ConsoleCommand((ConsoleCommandDelegate)d, attr.CommandName, attr.Description, attr.UsageString));
						else _commands.Add(new ConsoleCommand((ConsoleCommandDelegate)d, attr.CommandName, attr.Description));
					}
					catch (Exception ex)
					{
						Log.Write(MessageType.Error, "Cannot Register Command {0}.{1} because {2}", type.Name, mInfo.Name, ex.Message);
					}
				}

				_commands.Sort();
				
				Log.Write(MessageType.Status, "Commands Registered for {0}...", type.Name);
			}

			public void RegisterCommand(ConsoleCommandDelegate CallbackMethod, string CommandName, string Description, string UsageString)
			{
				ConsoleCommand cc = new ConsoleCommand(CallbackMethod, CommandName, Description, UsageString);
				if (!_commands.Contains(cc))
				{
					_commands.Add(cc);
					_commands.Sort();
				}
				else
				{
					Log.Write(MessageType.MinorError, "Command already exists: '{0}'", cc.CommandName);
				}
			}
			public void RegisterCommand(ConsoleCommandDelegate CallbackMethod, string CommandName, string Description)
			{
				ConsoleCommand cc = new ConsoleCommand(CallbackMethod, CommandName, Description);
				if (!_commands.Contains(cc))
				{
					_commands.Add(cc);
					_commands.Sort();
				}
			}

			internal string CommandReceived(string str)
			{
				string retval = "";
				Log.Write(MessageType.Cmd, "Received Command from Pipe. Command:{0}",str);

				try
				{
					string cmd = str;
					string[] args = null;
					if (str.IndexOf(' ') > 0)   // If command has parameters, break them up
					{
						cmd = str.Substring(0, str.IndexOf(' '));
						//args = str.Substring(str.IndexOf(' ') + 1).Split(' ');

						// Split arguments on whitespace, except if quoted:
						args = Regex.Matches(str.Substring(str.IndexOf(' ') + 1), @"[\""].+?[\""]|[^ ]+")
							 .Cast<Match>()
							 .Select(m => m.Value)
							 .ToList().ToArray<string>();

						// Cleanup the quotes
						for (int i = 0; i < args.Length; i++)
						{
							args[i] = args[i].Replace("\"", "");
						}
					}

					// Check for help(?) flag
					if (args != null && (str.Contains("/?") || str.Contains("help")))
					{
						//ConsolePipe cp = (ConsolePipe)_pipes[_pipes.IndexOf(new ConsolePipe(PipeID))];
						if (_commands != null)
						{
							retval = "Unrecognized Command: '" + cmd + "', try again.";
							foreach (ConsoleCommand cc in _commands)
							{
								if (cc.CommandName.ToLower() == cmd.ToLower())
									retval = cc.UsageString;
							}
						}
						else
						{
							retval = "Logger not yet configured\r\n";
						}
					}
					else
					{
						if (args == null) args = new string[0];
						retval = "Logger not yet configured\r\n";
						if (_commands != null && _commands.Count > 0)
						{
							retval = "";
							foreach (ConsoleCommand cc in _commands)
							{
								if (cc.CommandName.ToLower() == cmd.ToLower())
								{
									if (cc.CallbackMethod != null)
									{
										var r = cc.CallbackMethod(args);
										if(!string.IsNullOrWhiteSpace(r)) retval += r + "\r\n";
									}
								}
							}
						}
					}

				}
				catch (Exception ex)
				{
					Log.Write(MessageType.Error, "Error processing Cmd. Error: {0}", ex.Message);
				}
				if (retval == null) retval = "Unrecognized Command: '" + str + "'";
				StringBuilder sb = new StringBuilder();
				sb.AppendLine(retval);
				sb.AppendLine(";"); // command terminating line
				return sb.ToString();

			}

			private string CmdHelp(string[] args)
			{
				StringBuilder sb = new StringBuilder();
				foreach (ConsoleCommand cc in _commands)
				{
					sb.AppendLine(cc.CommandName.PadRight(18) + " - " + cc.Description);
				}
				return sb.ToString();
			}

			public void Dispose()
			{
				lock (_commands)
				{
					foreach (ConsoleCommand cmd in _commands) cmd.Dispose();
					_commands.Clear();
				}
			}
		}
	}
}


