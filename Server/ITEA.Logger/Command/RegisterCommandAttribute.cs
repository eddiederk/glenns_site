﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ITEA.Logger
{
	[System.AttributeUsage(System.AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class RegisterCommandAttribute : Attribute
	{
		public RegisterCommandAttribute()
		{

		}

		public RegisterCommandAttribute(string Description)
		{
			this.Description = Description;
		}

		public RegisterCommandAttribute(string CommandName, string Description)
		{
			this.CommandName = CommandName;
			this.Description = Description;
		}

		public RegisterCommandAttribute(string CommandName, string Description, string UsageString)
		{
			this.CommandName = CommandName;
			this.Description = Description;
			this.UsageString = UsageString;
		}

		public string CommandName { get; set; }
		public string Description { get; set; }
		public string UsageString { get; set; }
	}
}
