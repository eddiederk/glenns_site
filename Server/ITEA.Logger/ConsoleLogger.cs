﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using System.IO.Pipes;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using ITEA.Logger.Listeners;
using ITEA.Logger.Config;
using System.Xml;
namespace ITEA.Logger
{
	public class Logger : IDisposable
	{
		#region Parameters
		private bool disposed;
		public readonly string strMyName;
		private string strMyPath;
		private bool _includePipes;
		#endregion

		#region Properties
		public bool IsVerboseSet { get { return (EnabledFlags & MessageType.Verbose) == MessageType.Verbose; } }
		public bool IsDebuggingSet { get { return (EnabledFlags & MessageType.Debugging) == MessageType.Debugging; } }
		public long EnabledFlags { get; private set; }
		public LogOptions EnabledOptions { get; private set; }
		public string RootFolderName { get { return strMyPath; } }
		public bool MessageTypeIsEnabled(MessageType messageType) { return (EnabledFlags & messageType) == messageType; }

		public LogListenerCollection Listeners { get; private set; }
		internal Command.CommandManager CommandManager;
		public Timer refreshConfigTimer;
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes the Logger for operation. The Logger instance is added as static member to the Log class and accessible from static methods.
		/// </summary>
		/// <param name="MyName">Application name</param>
		/// <param name="MyPath">Application location</param>
		/// <param name="messageType">Passing an extended or derived MessageType value will force the values to instantiate</param>
		public Logger(string MyName, string MyPath, MessageType messageType) : this(MyName, MyPath, true) { }
		
		/// <summary>
		/// Initializes the Logger for operation. The Logger instance is added as static member to the Log class and accessible from static methods.
		/// </summary>
		/// <param name="MyName">Application name</param>
		/// <param name="MyPath">Application location</param>
		/// <param name="messageType">Passing an extended or derived MessageType value will force the values to instantiate</param>
		/// <param name="IncludePipes">Indicate whether remote pipe messaging will be enabled. Default is 'true'.</param>
		public Logger(string MyName, string MyPath, MessageType messageType, bool IncludePipes) : this(MyName, MyPath, IncludePipes) { }

		/// <summary>
		/// Initializes the Logger for operation. The Logger instance is added as static member to the Log class and accessible from static methods.
		/// </summary>
		/// <param name="MyName">Application name</param>
		/// <param name="MyPath">Application location</param>
		/// <param name="messageType">Passing an extended or derived MessageType value will force the values to instantiate</param>
		/// <param name="IncludePipes">Indicate whether remote pipe messaging will be enabled. Default is 'true'.</param>
		public Logger(string MyName, string MyPath, bool IncludePipes)
		{
			//if (messageType == null  || messageType.GetType() == typeof(MessageType)) throw new Exception("Oops!");
			_includePipes = IncludePipes;
			strMyName = MyName;
			strMyPath = MyPath;

			this.Listeners = new LogListenerCollection(this);
			try
			{
				this.LoadConfig();
			}
			catch (Exception ex)
			{ 
			
			}
			// Assign to static Log
			Log l = new Log(this);

			//Create Command Manager
			CommandManager = new Command.CommandManager();
			if (IncludePipes)
			{	//Only if IncludePipes is true. Otherwise allow command manager to send error message back.
				CommandManager.AutoRegisterCommands(this);
				//AutoRegister Listener Commands
				foreach (var k in this.Listeners) { CommandManager.AutoRegisterCommands(k); }
			}

			//Refresh Config RightNow and every 5 minutes after.
			refreshConfigTimer = new Timer(UpdateConfiguration, null, TimeSpan.Zero, TimeSpan.FromMinutes(5));
		}

		/// <summary>Initializes the Logger for operation. The Logger instance is added as static member to the Log class and accessible from static methods.</summary>
		/// <typeparam name="T">Auto load custom MessageType Class into MessageType</typeparam>
		/// <param name="MyName">Application name</param>
		/// <param name="MyPath">Application location</param>
		/// <param name="IncludePipes">Indicate whether remote pipe messaging will be enabled. Default is 'true'.</param>
		/// <returns>Logger Instance</returns>
		public static Logger Create<T>(string MyName, string MyPath, bool IncludePipes = true) where T : MessageType
		{
			MessageType.Load<T>();
			return new Logger(MyName, MyPath, IncludePipes);
		}

		/// <summary>Initializes the Logger for operation. The Logger instance is added as static member to the Log class and accessible from static methods.</summary>
		/// <typeparam name="T">Auto load custom MessageType Class into MessageType</typeparam>
		/// <param name="MyName">Application name</param>
		/// <param name="MyPath">Application location</param>
		/// <param name="RegisterCommands">Auto Register Commands From this Object</param>
		/// <returns>Logger Instance</returns>
		public static Logger Create<T>(string MyName, string MyPath, object RegisterCommands) where T : MessageType
		{
			Logger l = Create<T>(MyName, MyPath, true);
			l.CommandManager.AutoRegisterCommands(RegisterCommands);
			//CommandFactory.RegisterAllCommands();
			return l;
		}

		public static Logger Create<T>([System.Runtime.CompilerServices.CallerMemberName] string callerName = "") where T : MessageType
		{
			MessageType.Load<T>();
			return new Logger(callerName, AppDomain.CurrentDomain.BaseDirectory, false);
		}

		public static Logger Create(bool IncludePipes = false, string callerName = "")
		{
			MessageType.Load<MessageType>();
			if(string.IsNullOrEmpty(callerName)) callerName = Generics.MyName();
			return new Logger(callerName, Generics.MyPath(), IncludePipes);
		}

		#endregion

		~Logger()
		{
			Dispose(false);
		}
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{ // Release Managed Resources.
					refreshConfigTimer.Dispose();
					foreach (ILogListener l in this.Listeners) l.Dispose();
					CommandManager.Dispose();
				}
				// Release unmanaged resources.				
			}
			disposed = true;
		}

		#region LogMessage Methods
		public void LogMessage() { }

		public void LogMessage(string strMessage, MessageType messageType, bool ForceFlush) 
		{
			var d = CreateLogData(null, messageType, null, strMessage);
			if (d != null) Write<int>(null, d, ForceFlush);
		}
		public void LogMessage(string strMessage, MessageType messageType) { LogMessage(strMessage, messageType, false); }
		public void LogMessage(string strMessage, MessageType messageType, bool ForceFileWrite, bool WriteAppLogEntry)
		{
			if (WriteAppLogEntry)
			{
				try
				{
					Generics.WriteApplicationLogEntry(GetFormattedMessage(strMessage, messageType), strMyName);
				}
				catch (Exception ex)
				{
					LogMessage(MessageType.MinorError, ex, "Failed to Write to ApplicationLog");
				}
			}

			LogMessage(strMessage, messageType, ForceFileWrite);
		}

		public void LogMessage(string strMessage, MessageType messageType, Exception ex) { WriteMessageArgs<int>(null, messageType, ex, strMessage); }
		public void LogMessage(string strMessage) { LogMessage(strMessage, MessageType.Default, false); }

		#region New LogMessage Methods
		public void LogMessage(MessageType messageType, string strMessage, params object[] args) { WriteMessageArgs<int>(null, messageType, null, strMessage, args); }
		public void LogMessage(MessageType messageType, Exception ex, string strMessage, params object[] args) { WriteMessageArgs<int>(null, messageType, ex, strMessage, args); }

		public void LogMessageAs<T>(MessageType messageType, Exception ex, string strMessage, params object[] args) where T:ILogSource
		{ 
			WriteMessageArgs<int>(null, messageType, ex, strMessage, args); 
		}

		/// <summary>Write Log Message to a specific listener</summary>
		/// <typeparam name="T">Specifys which listener to write to.</typeparam>
		public void LogMessageTo<T>(MessageType messageType, string strMessage, params object[] args) where T:ILogListener
		{
			WriteMessageArgs<T>(null, messageType, null, strMessage, args); 
		}
		
		/// <summary>Write Log Message to a specific listener</summary>
		/// <typeparam name="T">Specifys which listener to write to.</typeparam>
		public void LogMessageTo<T>(MessageType messageType, Exception ex, string strMessage, params object[] args) where T:ILogListener
		{
			WriteMessageArgs<T>(null, messageType, ex, strMessage, args);
		}


		

		#endregion

		#endregion

		public static string GetExceptionMessage(Exception ex, bool ShowStackTrace)
		{
			System.Text.StringBuilder sb = new StringBuilder(1000);

			string stackTrace = ex.StackTrace;

			while (ex != null)
			{
				sb.AppendLine(ex.Message + "\r\n");
				ex = ex.InnerException;
			}

			if (ShowStackTrace) sb.AppendLine("StackTrace:" + stackTrace);

			return sb.ToString().Replace('{','[').Replace('}',']');
		}
		private string GetFormattedMessage(string strMessage, MessageType messageType)
		{
			DateTime now = DateTime.Now;
			return now.ToString("M/d/yy HH:mm:ss.fff") + ": " + messageType.Character + " " + strMessage;
		}
		/// <summary>Helper method to format the message using Args then calls Write.</summary>
		internal void WriteMessageArgs<T>(string source, MessageType messageType, Exception ex, string strMessage, object[] args = null)
		{
			var d = CreateLogData(source, messageType, ex, strMessage, args);
			if (d != null) Write<T>(source, d, false);
		}

		private LogData CreateLogData(string source, MessageType messageType, Exception ex, string strMessage, object[] args = null)
		{
			if (!Generics.HasABitMatch(EnabledFlags, messageType)) return null; //Simple Short Curcit.

			var m = new LogData()
			{
				MessageType = messageType,
				Message = strMessage,
				Args = args,
			};

			if (EnabledOptions.HasFlag(LogOptions.ProcessId)) m.ProcessId = System.Diagnostics.Process.GetCurrentProcess().Id;
			if (EnabledOptions.HasFlag(LogOptions.ThreadId)) m.ThreadId = System.Threading.Thread.CurrentThread.ManagedThreadId;

			if (ex != null)
			{	// Extract Exception Message
				m.Message += " Exeption: " + GetExceptionMessage(ex, false);
				if (EnabledOptions.HasFlag(LogOptions.CallstackError)) m.CallStack = ex.StackTrace;
			}
			else if (this.IsVerboseSet && EnabledOptions.HasFlag(LogOptions.CallstackVerbose))
			{	// Pull StackTrace
				m.CallStack = System.Environment.StackTrace;
			}
			return m;

		}

		private void Write<T>(string source, LogData m, bool flush)
		{
			bool ToEveryone = typeof(T) == typeof(int);
			bool IncludedLogSource = source != null;

			for (int k = 0; k < this.Listeners.Count; k++ )
			{
				var l = this.Listeners[k];
				if (l.IsDisposed)
				{	//Remove Disposed Listener from the collection;
					k--;
					this.Listeners.Remove(l);
					continue;
				}

				if (ToEveryone || l is T)
				{
					if (Generics.HasABitMatch(l.Settings.TraceLevel, m.MessageType))
					{
						try
						{
							l.Write(m);
							if (flush) l.Flush();
						}
						catch (Exception ex)
						{
							string msg = GetFormattedMessage(" Error Writting Message To '" + l.Name + "' Listener. Info: " + ex.Message, MessageType.Error);
							try
							{
								Generics.WriteApplicationLogError(msg, strMyName);
							}
							catch (Exception) { }
						}
					}
				}
			}
		}

		public void Flush()
		{
			for (int k = 0; k < this.Listeners.Count; k++)
			{
				var l = this.Listeners[k];
				if (l.IsDisposed)
				{	//Remove Disposed Listener from the collection;
					k--;
					this.Listeners.Remove(l);
					continue;
				}

				try { l.Flush(); }
				catch (Exception ex)
				{
					string msg = GetFormattedMessage(" Error Writting Message To '" + l.Name + "' Listener. Info: " + ex.Message, MessageType.Error);
					try { Generics.WriteApplicationLogError(msg, strMyName); }
					catch (Exception) { }
				}
			}
		}

		private void UpdateConfiguration(object state)
		{
			try
			{
				if (!File.Exists(RootFolderName + @"\Logger.config"))   // Create new logger.config file
				{
					this.Listeners.Add(new FileListener());
					if(_includePipes) this.Listeners.Add(new Pipes.PipeListener());
					this.SaveConfig();
				}
				else                                                    // Load existing Logger.config file
				{
					this.LoadConfig();
				}

				//Set EnabledFlags For improved memory usage.
				this.EnabledFlags = 0;
				this.EnabledOptions = LogOptions.None;

				foreach (ILogListener l in this.Listeners)
				{
					this.EnabledFlags |= l.Settings.TraceLevel;
					this.EnabledOptions |= l.Settings.Options;
				}
			}
			catch (Exception ex)
			{
				string strError = strMyName + " failed loading Logger.config. All Tracing will be left on. Info:" + ex.Message;
				LogMessage(strError, MessageType.Error, false);
			}
		}

		#region Commands
		[RegisterCommand("ltrace", "Display the trace levels for Logging", "ltrace [file|console|pipe]")]
		private string CmdDisplayTracingLevels(string[] args)
		{
			StringBuilder sb = new StringBuilder();
			ILogListener listener = null;

			if (args != null && args.Length > 0)
				listener = Listeners[args[0]];// LogListeners.FirstOrDefault(x => x.Name.ToLower() == args[0].ToLower());

			if (listener == null)
			{
				foreach (ILogListener l in Listeners)
				{
					sb.AppendFormat("{0} 0x{1} ({2})\r\n", l.Name.PadRight(10), l.Settings.TraceLevel.ToString("X").PadRight(10), l.Settings.TraceLevel.ToString());
				}
			}
			else
			{

				foreach (MessageType mType in MessageType.GetValues())
				{
					if ((listener.Settings.TraceLevel & mType) > 0)
						sb.AppendLine(mType.Name.PadRight(16) + mType.Character + " - On");
					else
						sb.AppendLine(mType.Name.PadRight(16) + mType.Character + " - Off");
				}
			}

			sb.AppendLine("");
			return sb.ToString();
		}

		[RegisterCommand("trace", "Set trace levels for Logging", "trace file|console|pipe [MessageType|wildcard] [\\on|\\off]")]
		private string CmdSetTracingLevels(string[] args)
		{
			// trace file|Console|Pipe [TraceName|*] [\on|\off] 
			StringBuilder sb = new StringBuilder();

			string type = "";
			bool onoff = false;
			long traceBits = 0;
			if (args != null)
			{
				type = args[0];
				if (args.Length > 2)
				{
					if (args[1] == "*")
					{
						traceBits = 0xFFFFFFFFFFFFFFF;
					}
					else
					{
						//traceBits = (long)GetLogMessageTypeFromName(args[1]);
						traceBits = MessageType.FromName(args[1]);
					}
					if (args[2].ToLower() == "\\on" || args[2].ToLower() == "on")
						onoff = true;
				}

				var listener = Listeners[type];// LogListeners.FirstOrDefault(x => x.Name.ToLower() == type.ToLower());
				if (listener != null)
				{
					if (onoff)
						listener.Settings.TraceLevel |= traceBits;
					else
						listener.Settings.TraceLevel -= traceBits & listener.Settings.TraceLevel;

					return CmdDisplayTracingLevels(new string[] { type });
				}
				else
				{
					sb.AppendLine("Unrecognized Log Type '" + type + "'");
					sb.AppendLine(@"Expecting: trace file|Console|Pipe [TraceName|*] [\on|\off] ");
				}
			}
			else
			{
				sb.Append("expecting ");
				foreach (ILogListener l in Listeners)
				{
					sb.Append(l.Name + " | ");
				}
				sb.Length = sb.Length - 2;
			}
			return sb.ToString();
		}

		[RegisterCommand("save", "Save logging configuration to file", "save")]
		private string CmdSaveConfigFile(string[] args)
		{
			try
			{
				SaveConfig();
			}
			catch (Exception ex)
			{
				LogMessage("Error saving Logger configuration file. Error:" + ex.Message, MessageType.Error);
				return "Error saving Logger configuration file. Error:" + ex.Message;
			}
			return "Config Saved"; // sb.ToString();
		}

		[RegisterCommand("FlushLog", "Forces the log file to be written.")]
		private string CmdFlushLogs(string[] args)
		{
			Log.FlushLogs(); //LogMessage("Writing log file...", MessageType.Cmd, true);   // Forces log file to be written
			return "Flushed Listeners.";
		}

		[RegisterCommand("reload", "Reloads Logger settings from configuration file", "reload")]
		private string CmdReloadLogConfig(string[] args)
		{
			UpdateConfiguration(null);
			return "Reload Configuration complete.";
		}

		[RegisterCommand("config", "edit the listener configuration settings")]
		private string CmdListListeners(string[] args)
		{
			var sb = new StringBuilder();

			if (args != null && args.Length > 0)
			{
				var l = Listeners[args[0]];
				if (l == null)
				{
					sb.AppendLine("Unexpected Name '" + args[0] +"'");
					args = null; //Force all listeners to print.
				}
				else 
				{
					if(args.Length == 3)
					{//set value
						try
						{
							var p = l.Settings.GetProperty(args[1]);
							if (p != null) p.SetValue(l.Settings, Convert.ChangeType(args[2], p.PropertyType));
							else sb.AppendLine("Unexpected PropertyName");
						}
						catch (Exception ex)
						{
							sb.AppendLine("Failed to set Property: " + ex.Message);
						}
					}

					// List Settings and Their Values 
					foreach (var s in l.Settings.GetSettings())
					{
						sb.AppendFormat("{0}{1}\r\n", s.Key.PadRight(20), s.Value);
					}
				}
			} 
			else 
			{
				sb.Append(" Expecting:");
				foreach (ILogListener l in Listeners) sb.Append(l.Name +"|");
				sb.Length = sb.Length - 1;
			}


			return sb.ToString();
		}
		#endregion

		public void SaveConfig(bool backup = false)
		{
			string path = Path.Combine(RootFolderName, "logger.config");

			if (backup)
			{ 
				string destination = Path.GetFileNameWithoutExtension(path) + Stopwatch.GetTimestamp() + Path.GetExtension(path);
				File.Copy(path, destination);
			}

			XmlWriterSettings xmlSet = new XmlWriterSettings()
			{
				Indent = true,
				NewLineHandling = NewLineHandling.Entitize
			};
			using (XmlWriter w = XmlWriter.Create(path, xmlSet))
			{
				w.WriteStartElement("Logging");
				w.WriteAttributeString("Version", ConfigFactory.VERSION);
				w.WriteStartElement("Tracing");
				foreach (ILogListener l in Listeners)
				{
					w.WriteStartElement("Listener");
					var t = l.GetType();
					w.WriteAttributeString("Name", l.Name ?? t.Name);
					int verIndex = t.AssemblyQualifiedName.IndexOf(", Ver");
					w.WriteAttributeString("Type", t.AssemblyQualifiedName.Substring(0,verIndex));

					if (l.Settings != null)
					{
						l.Settings.WriteXMLElements(w);
					}

					w.WriteEndElement();//Listener
				}
				w.WriteEndElement();//Listeners
				w.WriteEndElement();//Settings

				w.Flush();
			}
		}

		public void LoadConfig()
		{
			XmlReaderSettings s = new XmlReaderSettings() { IgnoreComments = true, IgnoreWhitespace = true };
			using (XmlReader r = XmlReader.Create(Path.Combine(RootFolderName, "logger.config")))
			{
				while (r.Read())
				{
					if (r.NodeType != XmlNodeType.Element) continue;
					if (r.Name.ToLower() == "listener")
					{
						var l = this.LoadListener(r);
						//You have to add it AFTER the listener settings have been updated.
						if (l !=null && !Listeners.Contains(l)) Listeners.Add(l);
					}
					else
					{	// Config Version Checking
						var version = r.GetAttribute("Version");
						if (r.Name.ToLower() == "loggerconfiguration") version = "1";

						if (version != null && version != ConfigFactory.VERSION)
						{
							Console.WriteLine("Logger.config is old. Upgrading.");
							var arr = this.UpgradeFromOldConfig(version, r);
							if (arr == null) continue;//Upgrade doesn't exist.
							
							foreach (var l in arr)
							{
								if (!Listeners.Contains(l)) Listeners.Add(l);
							}
							Log.Write(MessageType.Status, "Successfully Upgraded Logger.config");
							r.Close();
							SaveConfig(true); //Update File.
							return;
						}
					}
				}
			}
		}
	}
}
