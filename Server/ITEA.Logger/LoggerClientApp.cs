﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Security;
using System.Diagnostics;

namespace ITEA.Logger
{
	class LoggerClientApp
	{
		private static LoggerClient pipe;
		private static int minutesTimeout=15;
		private static string title;
		private static string appname;

		static void Main(string[] args)
		{
			Thread.Sleep(1000);

			appname = Generics.MyName();
			title = appname;
			
			if (args.Length > 0)
			{
				appname = args[0];
				var arr = appname.Trim('"').Split(new char[] { '/', '\\', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
				if (arr.Length >= 2)
				{
					appname = arr.Last();
					title = args[0].Substring(0, args[0].IndexOf(appname) - 1);
				}
			}
			if(args.Length > 1) int.TryParse(args[1], out minutesTimeout);

			Console.WriteLine(string.Format(@"Command Args: [0]='{0}'(title/pipe) [1]={1}(connect timeout in minutes)", appname, minutesTimeout));

			try
			{
				while (true)
				{
					Connect(appname);
					Listen();
				}
			}
			catch (SecurityException)
			{
				Console.Write("Permission Denied. You Must run as Administrator");
			}
		
		}

		static private void Connect(string appname)
		{
			//Console.WriteLine("My name is " + appname);
			Timer timeoutTimer = null;
			if (minutesTimeout > 0)
				timeoutTimer = new Timer((x)=> Environment.Exit(-101), null, TimeSpan.FromMinutes(minutesTimeout), TimeSpan.Zero);
			
			pipe = new LoggerClient(appname);
			pipe.MessageReceived += new MessageReceivedHandler(LogMessage);
			pipe.TitleUpdate += new TitleUpdateHandler(UpdateTitle);
			Thread threadPipe = new Thread(pipe.RunOperations);
			threadPipe.Start(PipeState.OutputMode);

			// Wait for application to come alive up to 15 minutes
			int k = 0;
			UpdateTitle("Connecting");
			Stopwatch w = Stopwatch.StartNew();
			while (pipe.State != PipeState.CommandMode && pipe.State != PipeState.OutputMode)
			{
				UpdateTitle(string.Format("Connecting>{0}", TimeSpan.FromMilliseconds(w.ElapsedMilliseconds).ToString(@"hh\:mm\:ss")));
				switch (k++ % 5)
				{
					case 1: Console.Write("\rAttempting to connect to pipe:'" + appname + "' /"); break;
					case 2: Console.Write("\rAttempting to connect to pipe:'" + appname + "' -"); break;
					case 3: Console.Write("\rAttempting to connect to pipe:'" + appname + "' |"); break;
					case 4: Console.Write("\rAttempting to connect to pipe:'" + appname + "' \\"); break;
				}
				Thread.Sleep(1000);
			}

			if(timeoutTimer != null) timeoutTimer.Dispose();
			
			pipe.SendCommand("start");
		}

		static private void Listen()
		{
			string temp;
			try
			{
				while (pipe.State != PipeState.Closed) //&& (temp = Console.ReadLine()) != null)
				{
					if (Console.KeyAvailable)
					{
						temp = Console.ReadLine();

						if (pipe.State == PipeState.Closed)
						{
							break;
						}

						pipe.State = PipeState.CommandMode;

						//bool executingCmd = false;
						switch (temp.ToLower().Trim())
						{
							case "start":
								pipe.State = PipeState.OutputMode;
								pipe.SendCommand("start");
								Console.Clear();
								Connect(appname);
								//_swPipe.WriteLine("start");
								break;
							case "close":
								pipe.Close();
								break;
							case "exit":
								pipe.Close();
								break;
							case "connect":
								Console.WriteLine("Not yet implemented");
								break;
							case "":
								pipe.State = PipeState.CommandMode;
								pipe.SendCommand("stop");
								//_swPipe.WriteLine("stop");
								break;
							default:
								pipe.State = PipeState.CommandMode;
								pipe.executingCmd = true;
								pipe.SendCommand(temp);
								//_swPipe.WriteLine(temp);
								break;
						}

						if (pipe.State == PipeState.CommandMode)
						{
							int seconds = 0;
							Thread.Sleep(500);
							while (pipe.executingCmd && seconds < 29)    // Wait for any remaining messages before displaying command prompt
							{
								Console.Write('.');
								Thread.Sleep(500);
								seconds++;
							}

							Thread.Sleep(500);
							//RaiseMessage("Cmd:>");
							//Console.WriteLine("");
							Console.Write("Cmd:>");  // Display Command prompt
							pipe.executingCmd = false;
						}

					}

					Thread.Sleep(11);
				}

				Console.WriteLine("Pipe Closed.");
				
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error Received. Error:" + ex.Message);
				Thread.Sleep(10000);

				pipe.State = PipeState.Closed;
			}
		}

		static private void LogMessage(string message)
		{
			Console.WriteLine(message);
		}
		static private void UpdateTitle(string message)
		{
			Console.Title = string.Format("LGR[{0}] - {1}", title, message);
		}
	}
}
