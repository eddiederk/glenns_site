﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Security.Permissions;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Runtime.InteropServices;

namespace ITEA.Logger
{

	public delegate void MessageReceivedHandler(string Message);
	public delegate void TitleUpdateHandler(string Message);
	public class LoggerClient
	{
		public event MessageReceivedHandler MessageReceived;
		public event TitleUpdateHandler TitleUpdate;

		private NamedPipeClientStream _pipeMessages;
		private NamedPipeClientStream _pipeCommands;
		private StreamReader _srPipe;
		private StreamWriter _swPipe;
		private string _appName = "Debug";
		private int _id;
		private PipeState _state;
		public bool executingCmd = false;

		#region Constructors
		public LoggerClient() : this("Debug") { }

		public LoggerClient(string AppName) //: this (true)
		{
			this._appName = AppName;
		}

		#endregion

		#region Properties
		public PipeState State
		{
			get
			{
				if (_pipeCommands == null || !_pipeCommands.IsConnected)
					_state = PipeState.Closed;

				return _state;
			}
			set
			{
				_state = value;
			}
		}

		#endregion

		public void Close()
		{
			_state = PipeState.Closed;
			try
			{
				_swPipe.WriteLine("close " + _id.ToString());
			}
			catch (Exception ex)
			{
				RaiseMessage("Error: " + ex.Message);
			}
			finally
			{
				if (_pipeCommands != null)
				{
					if (_pipeCommands.IsConnected)
						_pipeCommands.Close();
				}
				if (_pipeMessages != null)
				{
					if (_pipeMessages.IsConnected)
						_pipeMessages.Close();
				}
			}
		}

		public void RunOperations(object o)
		{
			PipeState StartState = (PipeState)o;
			try
			{
				ConnectToApp(StartState);

				if (StartState == PipeState.OutputMode)
					_swPipe.WriteLine("start");

				UpdateTitle("Pipe ID:" + _id.ToString() + " - connection:" + _pipeMessages.NumberOfServerInstances.ToString());

				string temp = "";
				while (_pipeMessages.IsConnected && (temp = _srPipe.ReadLine()) != null)
				{
					if (_state == PipeState.CommandMode && temp == ";")    // Command terminating line character
						executingCmd = false;

					else if (temp.StartsWith("BANG!"))
					{
						RaiseMessage(temp);
						Thread.Sleep(1000);
						Close();
					}

					else if (_state != PipeState.Closed)
					{
						if (_state == PipeState.OutputMode || _state == PipeState.CommandMode)
						{
							RaiseMessage(temp);
						}
					}
				}

			}
			catch (System.Security.SecurityException ex)
			{
				RaiseMessage("You Must run w/ Admin Privileges\r\n" + ex.Message);
			}
			catch (Exception ex)
			{
				RaiseMessage("ERROR: " + ex.Message);
			}
			finally
			{
				Thread.Sleep(8001);
				if (!_pipeMessages.IsConnected)
				{
					_state = PipeState.Closed;
				}
			}

		}

		private void RaiseMessage(string message)
		{
			if (MessageReceived != null)
				MessageReceived(message);
		}
		private void UpdateTitle(string message)
		{
			if (TitleUpdate != null)
				TitleUpdate(message);
		}
#if !DEBUG
		//[PrincipalPermission(SecurityAction.Demand, Role = @"BUILTIN\Administrators")]
#endif
		public void ConnectToApp(PipeState StartState)
		{
			_pipeMessages = new NamedPipeClientStream(".", _appName, PipeDirection.In);
			_srPipe = new StreamReader(_pipeMessages);
			// RaiseMessage("Attempting to connect.");
			
			
			while (true)
			{
				if (DoesNamedPipeExist(_appName))
				{
					try
					{
						_pipeMessages.Connect(1000);
						break;
					}
					catch (TimeoutException) { }
				}
				Thread.Sleep(100);
			}

			// _pipeMessages.SetAccessControl(pipeSa);
			RaiseMessage("Connected");
			_state = PipeState.Configuring;

			_id = Convert.ToInt32(_srPipe.ReadLine());  // Read PipeID


			RaiseMessage("Setting up command pipe....");
			_pipeCommands = new NamedPipeClientStream(".", _appName + _id.ToString(), PipeDirection.Out);
			_pipeCommands.Connect(3000);
			RaiseMessage("Command Pipe is connected!!!");
			_swPipe = new StreamWriter(_pipeCommands);
			_swPipe.AutoFlush = true;
			_state = StartState; // PipeState.OutputMode;

		}

		public void SendCommand(string command)
		{
			try
			{
				if (_pipeCommands != null || _pipeCommands.IsConnected)
				{
					_swPipe.WriteLine(command);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error sending commad: " + ex.Message);
			}
		}



		[return: MarshalAs(UnmanagedType.Bool)]
		[DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
		private static extern bool WaitNamedPipe(string name, int timeout);


		/// <summary>
		/// Method to test if Windows considers that a named pipe of a certain name exists or not.
		/// </summary>
		internal static bool DoesNamedPipeExist(string _appName)
		{
			try
			{
				return WaitNamedPipe(@"\\.\pipe\" + _appName, 0);
			}
			catch (Exception)
			{
				return false;
			}
		}
	}
}
