﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
//using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ITEA.Logger
{
	public interface IMessageType { }
	/// <summary>
	/// Implements the Type-Safe Enum Pattern to provide MessageType definitions for logging.  Allows for inherited enum functionality.
	/// </summary>
	public class MessageType : IMessageType
	{
		public readonly string Name;
		public readonly string Character;
		public readonly long Value;

		private static ArrayList values;
		private Regex regexNumber = new Regex(@"\d+$");

		//New --- Simplify Implementation of MessageType inheritance
		// Also prevents exceptions from breaking the apps
		/// <summary>This is just to make inheritance easier and isn't used.</summary>
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
		protected MessageType() { throw new NotImplementedException("MessageType should NOT be instanciated;"); }
		
		/// <summary>Adds a Custom MessageType to the MessageType Instance</summary>
		/// <param name="value">Custom MessageTypes Flag ID</param>
		/// <param name="character">Custom MessageTypes LogFile Character</param>
		/// <param name="name">Custom MessageTypes Display Name</param>
		/// <returns>MessageType Instance</returns>
		protected static MessageType Add(long value, string character, string name)
		{
			try
			{
				return new MessageType(value, character, name);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Failed To Create MessageType: " + name + "[" + character + "] = " + value + "(0x" + value.ToString("X") + ")");
				Console.WriteLine(ex.Message);
				Console.WriteLine("Returning Existing Message Type");
				return MessageType.FromValue(value);
			}
		}	///----New
		
		/// <summary>Loads All Static MessageTypes from another class into the MessageType instance</summary>
		/// <typeparam name="T">Custom MessageType Class</typeparam>
		/// <remarks>This can be called multiple times without impact.</remarks>
		public static bool Load<T>() where T : IMessageType
		{
			var t = typeof(T);
			var p = t.GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
			if (p.Length > 0) p.First().GetValue(null);
			return true;
		}


		protected MessageType(long value, string character, string name)
		{
			if (values == null) values = new ArrayList();

			foreach (MessageType m in values)
			{
				//if (value == m.Value) throw new Exception("Invalid value provided for '"+ name +"' MessageType. " + value.ToString() + "(0x" + value.ToString("X") + ") already assigned.");
				if (character == m.Character)
				{
					Console.WriteLine("Invalid character provided for MessageType: " + name + " '" + character + "' already in use.");
					character = IncrementString(character);
					Console.WriteLine("Assigning character '" + character + "'");
					//throw new Exception("Invalid character provided for MessageType. '" + character + "' already in use.");
				}
				if (name == m.Name)
				{
					Console.WriteLine("Invalid Name provided for MessageType. '" + name + "' already in use.");
					name = IncrementString(name);
					Console.WriteLine("Assigning Name '" + name + "'");
					//throw new Exception("Invalid Name provided for MessageType. '" + name + "' already in use.");
				}
			}

			Value = value;
			Name = name;
			Character = character;

			values.Add(this);
		}

		public static Array GetValues() { return values.ToArray(typeof(MessageType)); }
		public static MessageType FromName(string name)
		{
			foreach (MessageType m in values)
			{
				if (m.Name == name)
					return m;
			}

			return null;
		}
		public static MessageType FromCharacter(string character)
		{
			foreach (MessageType m in values)
			{
				if (m.Character == character)
					return m;
			}

			return null;
		}
		public static MessageType FromValue(long value)
		{
			foreach (MessageType m in values)
			{
				if (m.Value == value)
					return m;
			}

			return null;
		}

		// Enables implicit casting as a long
		public static implicit operator long(MessageType m) { return m != null ? m.Value : 0; } // if (m != null) return m.Value; else return 0; }

		#region Enumerated values
		public static readonly MessageType Error =			new MessageType(0x00000001, "X", "Error");
		public static readonly MessageType MinorError =		new MessageType(0x00000002, "x", "MinorError");
		public static readonly MessageType Status =			new MessageType(0x00000004, "^", "Status");
		public static readonly MessageType Configuration = new MessageType(0x00000008, "c", "Configuration");
		//public static readonly MessageType LogFile =			new MessageType(0x00000010, "%", "Logfile");
		public static readonly MessageType Cmd =				new MessageType(0x00000020, ":", "Cmd");
		//public static readonly MessageType Pipe =				new MessageType(0x00000040, "|", "Pipe");
		public static readonly MessageType Verbose =			new MessageType(0x00000080, "~", "Verbose");
		public static readonly MessageType Debugging =		new MessageType(0x00000100, "'", "Debugging");
		public static readonly MessageType Default =			new MessageType(0x00000200, " ", "Default");
		public static readonly MessageType Warning =			new MessageType(0x00000400, "!", "Warning");
		public static readonly MessageType Timeout =			new MessageType(0x00000800, "t", "Timeout");
		public static readonly MessageType Database =		new MessageType(0x00001000, "db", "Database");
		#endregion

		private string IncrementString(string str)
		{
			if (!regexNumber.IsMatch(str)) return str + "2";

			var number = int.Parse(regexNumber.Match(str).Value);
			return str.Replace(number.ToString(), (number++).ToString());
		}
	}

	/// <summary>Instance of this class will auto register Custom MessageType</summary>
	/// <typeparam name="T">Custom MessageType Class</typeparam>
	public abstract class MessageTypeInit<T> where T : IMessageType
	{
		public MessageTypeInit()
		{
			MessageType.Load<T>();
		}
	}
}
