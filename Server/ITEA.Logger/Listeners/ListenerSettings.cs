﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ITEA.Logger.Listeners
{
	/// <summary>
	/// 
	/// </summary>
	public class ListenerSettings
	{
		#region Static/Const
		public const string DEFAULT_DATE_FORMAT = "M/d/yy HH:mm:ss.fff";
		private static long _dfltTraceLevel = -1;
		public static long DEFAULT_TRACE_LEVEL
		{
			get
			{
				if (_dfltTraceLevel == -1)
				{
					_dfltTraceLevel = 0;
					foreach (MessageType m in MessageType.GetValues())
					{
						_dfltTraceLevel += m.Value;
					}
				}

				return _dfltTraceLevel;
			}
		}

		public const LogOptions DEFAULT_OPTION = LogOptions.DateTime;
		#endregion

		[Description("Tracing")]
		public long TraceLevel { get; set; }
		public string DateTimeFormat { get; set; }
		public LogOptions Options { get; set; }

		public ListenerSettings()
		{
			TraceLevel = DEFAULT_TRACE_LEVEL;
			DateTimeFormat = DEFAULT_DATE_FORMAT;
			Options = LogOptions.DateTime;
		}
	}
}
