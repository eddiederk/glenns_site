﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ITEA.Logger.Listeners
{
	//TODO: File Compression Reader: how do we know what the codes are? They are different depending on the application... I think we need to store them in the dictionary file
	public class FileListener : LogListener<FileListenerSettings, FileListener.FileMessageType>
	{
		#region Fields
		private string strCurrentLogFileName = "";
		private string _loggerFolder = "";
		private string RootFolderName { get  { return this.Settings.WriteFolder ?? _loggerFolder; } }
		private int LogFileSizeMb { get { return this.Settings.FileSizeMb; } }
		private System.Timers.Timer tmrDeleteOldLogFiles;
		private System.Timers.Timer tmrFlushLogs;
		private IsWritingLogFile isWritingLogFile = new IsWritingLogFile();
		private System.Text.StringBuilder sbLogMessages;
		private string logName = "";
		#endregion
		
		#region Constructor

		public FileListener()
		{
			Name = "File";
			sbLogMessages = new StringBuilder(50000, 250000);
			StartTimers();
		}

		#endregion

		#region Abstract Methods
		public override void Initalize(Logger logger)
		{
			base.Initalize(logger);
			Debug.WriteLine(this.Settings.FileSizeMb);
			this.logName = logger.strMyName;
			this._loggerFolder = logger.RootFolderName + "\\log";
			
			LoadDictEntriesIfFileExists(); 
			CheckForLogFolder();

			//Delete Files Now
			Thread t = new Thread(DeleteOldLogFiles);
			t.Start();
			
		}

		public override void Write(LogData message)
		{
			string s = GetFormattedMessage(message);
			lock (sbLogMessages)
			{
				sbLogMessages.AppendLine(s);
			}

			if (sbLogMessages.Length > 150000)
			{
				lock (sbLogMessages)
				{
					sbLogMessages.Remove(0, 20000);
					LogMessage("Log Buffer exceed 50000 bytes. Deleted (lost) 20000 bytes.");
				}
			}
			else if (sbLogMessages.Length > 5000)  // Lets flush the Log Message and write to file using a new thread that leaves our app available for other logs
			{
				Flush();
			}
		}

		public override string GetFormattedMessage(LogData msg)
		{
			if (!Settings.Compress) return base.GetFormattedMessage(msg);
			string footer="", body, header = "";

			try
			{
				body = CompressMessage(msg.Message, msg.Args); ;
				
				if ((Settings.Options.HasFlag(LogOptions.CallstackError) || Settings.Options.HasFlag(LogOptions.CallstackVerbose)) && !string.IsNullOrEmpty(msg.CallStack)) 
					footer = CompressMessage("Callstack:" + msg.CallStack, null);
			}
			catch (Exception)
			{
				return base.GetFormattedMessage(msg);
			}

			if (Settings.Options.HasFlag(LogOptions.DateTime) && msg.DateTime > DateTime.MinValue) header += msg.DateTime.ToString(DateTimeFormat) + " ";

			header += "[ " + msg.MessageType.Character;

			if (Settings.Options.HasFlag(LogOptions.ThreadId) && msg.ThreadId > 0) header += " T:" + msg.ThreadId;

			if (Settings.Options.HasFlag(LogOptions.ProcessId) && msg.ProcessId > 0) header += " P:" + msg.ProcessId;

			return header + "]" + body + footer;
		}

		#region Log File Compression
		private static ConcurrentDictionary<int, int> MessageHashes = new ConcurrentDictionary<int, int>();
		private const string DICT_FILE = "LogDictionary.dat";
		private static string DICT_WRITE_FORMAT = char.ConvertFromUtf32(191) + "{0}:{1}" + char.ConvertFromUtf32(191) + "\r\n";
		private const string DICT_LOAD_FORMAT = @"\u00BF(?<key>\d+):(?<message>.*)\u00BF";
		private const string DICT_LOAD_MESSAGE = @"([\u00BF])(?:(?=(\\?))\2.)*?\1";
		private static string COMPRESS_FORMAT = char.ConvertFromUtf32(191) + "{0}:{1}" + char.ConvertFromUtf32(191);
		private static string COMPRESS_SEPERATOR = char.ConvertFromUtf32(161); //Inverted Exclamation Mark

		private void LoadDictEntriesIfFileExists()
		{
			if (!File.Exists(DICT_FILE)) return;

			string text = File.ReadAllText(DICT_FILE);
			
			Regex r = new Regex(DICT_LOAD_MESSAGE, RegexOptions.Singleline);
			Regex r2 = new Regex(DICT_LOAD_FORMAT, RegexOptions.Singleline);
			var matches = r.Matches(text);
			foreach (Match match in matches)
			{
				Match m2 = r2.Match(match.Value);
				int key = int.Parse(m2.Groups["key"].Value);
				string message = m2.Groups["message"].Value;
				//Load for Logging
				MessageHashes.TryAdd(message.GetHashCode(),key);
			}
		}
		
		private string CompressMessage(string message, object[] args)
		{
			int hash = message.GetHashCode();
			int id = 0;
			if (!MessageHashes.TryGetValue(hash, out id))
			{	// add new hash to dictionary
				MessageHashes.TryAdd(hash, MessageHashes.Count + 1);
				if (MessageHashes.TryGetValue(hash, out id))
				{	// append new message to dictionary file 
					File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DICT_FILE), string.Format(DICT_WRITE_FORMAT, id, message));
				}
			}

			if (id == 0) throw new ApplicationException();
			else
			{	// Create compressed log message.
				string values = "";
				if (args!=null && args.Length > 0)
				{
					var t = Enumerable.Range(0, args.Length).ToList();
					var valuesFormat = "{" + string.Join("}"+COMPRESS_SEPERATOR+"{", t) + "}";
					values = string.Format(valuesFormat, args);
				}
				
				return string.Format(COMPRESS_FORMAT, id, values);
			}
		}
		
		#endregion

		public override void Flush()
		{
			bool shouldWriteLogFile;

			lock (isWritingLogFile)
			{
				shouldWriteLogFile = ShouldWriteLogFile();
			}

			if (shouldWriteLogFile)
			{
				LogMessage("Beginning to write Log File.");

				System.Threading.Thread threadLogMessage = new Thread(new System.Threading.ThreadStart(WriteLogFile));
				threadLogMessage.Start();
			}
		}

		public override MessageType LogMessageType { get { return FileMessageType.LogFile; } }

		protected override void Dispose(bool disposing)
		{
			Flush();
			//sbLogMessages = null; This is a bad idea...because flush isn't done yet.
			base.Dispose(disposing);
		}
		#endregion

		private void CheckForLogFolder()
		{
			if (!System.IO.Directory.Exists(RootFolderName))
			{
				System.IO.Directory.CreateDirectory(RootFolderName);
			}
		}
		
		private string GetLogFileName()
		{
			string fileName = String.Format(RootFolderName + @"\{0}_", logName) + DateTime.Now.Year + "_" + Generics.IntToZeroPadString(DateTime.Now.Month, 2) + "_" + Generics.IntToZeroPadString(DateTime.Now.Day, 2) + "(" + Generics.IntToZeroPadString(DateTime.Now.Hour, 2) + "_" + Generics.IntToZeroPadString(DateTime.Now.Minute, 2) + ").txt";
			Debug.WriteLine("LogFile:"+fileName);
			return fileName;
		}

		private string GetLastLogFile()
		{
			var newestfile = Directory.GetFiles(RootFolderName, logName + "*").OrderByDescending(x=>x).FirstOrDefault();
			if (!string.IsNullOrEmpty(newestfile))
			{
				FileInfo fi = new FileInfo(newestfile);
				Debug.WriteLine("ExistingLogFile:" + newestfile);
				if (fi.LastWriteTime.Date == DateTime.Today) return newestfile;
			}
			
			return GetLogFileName();
		}

		private void WriteLogFile()
		{

			int intTicksAtStart = Environment.TickCount;

			try
			{
				if (strCurrentLogFileName == "") strCurrentLogFileName = GetLastLogFile();
				else
				{
					try
					{
						System.IO.FileInfo objFileInfo = new System.IO.FileInfo(strCurrentLogFileName);

						if (objFileInfo != null)
						{
							if (objFileInfo.Length >= this.LogFileSizeMb * 1048576)
							{
								strCurrentLogFileName = GetLogFileName();
							}

							objFileInfo = null;
						}
					}
					catch (Exception ex)
					{
						string strCurMessage = "Error finding file: {0} info: {1}";
						sbLogMessages.Append(string.Format(strCurMessage+"\r\n", strCurrentLogFileName,ex.Message));
						LogMessage(strCurMessage, strCurrentLogFileName, ex.Message);
					}
				}

				System.IO.FileStream fs = new System.IO.FileStream(strCurrentLogFileName, System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.None);

				byte[] bytLogMessage;
				lock (sbLogMessages)
				{
					bytLogMessage = System.Text.ASCIIEncoding.UTF8.GetBytes(sbLogMessages.ToString());
					sbLogMessages.Remove(0, sbLogMessages.Length);
				}

				fs.Write(bytLogMessage, 0, bytLogMessage.Length);

				fs.Close();
				fs = null;

				//string strCurLogMessage = "Finished writing Log File {0}. It took {1} ticks";// +strCurrentLogFileName + ". It took " + (Environment.TickCount - intTicksAtStart) + " ticks";
				//LogMessage(strCurLogMessage, LogMessageType.LogFile);
				LogMessage("Finished writing Log File {0}. It took {1} ticks", strCurrentLogFileName, Environment.TickCount - intTicksAtStart);
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Failed to Save To File");
				//string strErrorMessage = GetFormattedMessage(strMyName + " Error! Failure writing log file.  Info:" + ex.Message, LogMessageType.Error);
				string strErrorMessage = Name + " Error! Failure writing log file.  Info:" + ex.Message;

				if (!(sbLogMessages == null)) sbLogMessages.Append(strErrorMessage + "\r\n");
				try
				{
					Generics.WriteApplicationLogError(strErrorMessage, Name);
				}
				catch (Exception) { }
			}

			isWritingLogFile.Currently = false;
		}

		private void DeleteOldLogFiles()
		{
			System.IO.DirectoryInfo oLogDirectory = new System.IO.DirectoryInfo(RootFolderName);
			System.IO.FileInfo[] oFiles = oLogDirectory.GetFiles();

			for (int i = 0; i < oFiles.Length - 1; i++)
			{
				if (oFiles[i].LastWriteTime < DateTime.Now.AddHours(-Settings.HoursToKeepLogFiles))
				{
					//LogMessage("Deleting an old logfile: " + oFiles[i].Name, LogMessageType.LogFile);
					LogMessage("Deleting an old logfile: {0}", oFiles[i].Name);
					oFiles[i].Delete();
				}
			}
		}
		
		private void StartTimers()
		{
			tmrDeleteOldLogFiles = new System.Timers.Timer(3600000); // every hour
			tmrDeleteOldLogFiles.Elapsed += new System.Timers.ElapsedEventHandler(tmrDeleteOldLogFiles_Elapsed);
			tmrDeleteOldLogFiles.Enabled = true;

			tmrFlushLogs = new System.Timers.Timer(TimeSpan.FromMinutes(1).TotalMilliseconds);
#if DEBUG
			tmrFlushLogs.Interval = TimeSpan.FromSeconds(5000).TotalMilliseconds;
#endif
			tmrFlushLogs.Elapsed += new System.Timers.ElapsedEventHandler((a,b)=>WriteLogFile());
			tmrFlushLogs.Enabled = true;
		}
		
		private bool ShouldWriteLogFile()
		{
			if (!isWritingLogFile.Currently)
			{
				isWritingLogFile.Currently = true;
				return true;
			}
			else return false;
		}
		
		private void tmrDeleteOldLogFiles_Elapsed(object sender, System.Timers.ElapsedEventArgs e) { DeleteOldLogFiles(); }

		#region Commands
		[RegisterCommand("logduration", "Displays or updates the length, in hours, that logfiles will be retained on disk.", "logduration [duration]")]
		private string CmdLogDuration(string[] args)
		{
			string retval = "Keeping logs for " + Settings.HoursToKeepLogFiles.ToString() + " Hours";
			//StringBuilder sb = new StringBuilder();

			if (args != null)
			{
				if (args.Length >= 1)
				{
					try
					{
						Log.Write(FileMessageType.Default, "Setting HoursToHoldLogFiles to {0} hours", args[0]);
						this.Settings.HoursToKeepLogFiles = int.Parse(args[0]);

						LogMessage("Saving Logger confuration...");
						Log.SaveConfig();
						retval = "Configuration updated.\r\nKeeping logs for " + Settings.HoursToKeepLogFiles.ToString() + " Hours";
					}
					catch (Exception ex)
					{
						retval = "Error saving Logger configuration. Error:" + ex.Message;
						Log.Write(MessageType.Error, ex, "Error saving Logger configuration.");
					}
				}
			}
			
			return retval;
		}
		#endregion

		private class IsWritingLogFile
		{
			private bool isWritingLogFile = false;

			public bool Currently
			{
				get { return isWritingLogFile; }
				set { isWritingLogFile = value; }
			}

		}

		public class FileMessageType : MessageType
		{
			public static  MessageType LogFile = Add(0x00000010, "%", "Logfile");
		}
	}

	public class FileListenerSettings : ListenerSettings
	{
		public FileListenerSettings() 
		{
			HoursToKeepLogFiles = 48;
			FileSizeMb = 50;
		}

		public int HoursToKeepLogFiles { get; set; }
		public bool Compress { get; set; }
		public string WriteFolder { get; set; }
		public int FileSizeMb { get; set; }
	}
}
