﻿//
// System.Diagnostics.TraceListenerCollection.cs
//
// Authors:
//   Jonathan Pryor (jonpryor@vt.edu)
//
// Comments from John R. Hicks <angryjohn69@nc.rr.com> original implementation 
// can be found at: /mcs/docs/apidocs/xml/en/System.Diagnostics
//
// (C) 2002 Jonathan Pryor
//

//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//


using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;

namespace ITEA.Logger.Listeners
{

	public class LogListenerCollection : IList, ICollection, IEnumerable
	{
		private Logger parent;
		private ArrayList listeners = ArrayList.Synchronized(new ArrayList(1));

		internal LogListenerCollection(Logger logger) : this(logger, true) { }

		internal LogListenerCollection(Logger logger, bool addDefault)
		{
			this.parent = logger;
			if (addDefault)
			{
				Add(new ConsoleListener() { Name = "Console" });
			}
		}

		public int Count
		{
			get { return listeners.Count; }
		}

		public ILogListener this[string name]
		{
			get
			{
				lock (listeners.SyncRoot)
				{
					foreach (ILogListener listener in listeners)
					{
						if (listener.Name.ToLower() == name.ToLower())
							return listener;
					}
				}
				return null;
			}
		}

		public ILogListener this[int index]
		{
			get { return (ILogListener)listeners[index]; }
			set
			{
				InitializeListener(value);
				listeners[index] = value;
			}
		}

		object IList.this[int index]
		{
			get { return listeners[index]; }
			set
			{
				ILogListener l = (ILogListener)value;
				InitializeListener(l);
				this[index] = l;
			}
		}

		bool ICollection.IsSynchronized
		{
			get { return listeners.IsSynchronized; }
		}

		object ICollection.SyncRoot
		{
			get { return listeners.SyncRoot; }
		}

		bool IList.IsFixedSize
		{
			get { return listeners.IsFixedSize; }
		}

		bool IList.IsReadOnly
		{
			get { return listeners.IsReadOnly; }
		}

		public int Add(ILogListener listener)
		{
			InitializeListener(listener);
			return listeners.Add(listener);
		}

		private void InitializeListener(ILogListener listener)
		{
			listener.Initalize(parent);
		}

		private void InitializeRange(IList listeners)
		{
			int e = listeners.Count;
			for (int i = 0; i != e; ++i)
				InitializeListener(
					(ILogListener)listeners[i]);
		}

		public void AddRange(ILogListener[] value)
		{
			InitializeRange(value);
			listeners.AddRange(value);
		}

		public void AddRange(LogListenerCollection value)
		{
			InitializeRange(value);
			listeners.AddRange(value.listeners);
		}

		public void Clear()
		{
			listeners.Clear();
		}

		public bool Contains(ILogListener listener)
		{
			return listeners.Contains(listener);
		}

		public void CopyTo(ILogListener[] listeners, int index)
		{
			listeners.CopyTo(listeners, index);
		}

		public IEnumerator GetEnumerator()
		{
			return listeners.GetEnumerator();
		}

		void ICollection.CopyTo(Array array, int index)
		{
			listeners.CopyTo(array, index);
		}

		int IList.Add(object value)
		{
			if (value is ILogListener)
				return Add((ILogListener)value);
			throw new NotSupportedException("You can only add ILogListener objects to the collection");
		}

		bool IList.Contains(object value)
		{
			if (value is ILogListener)
				return listeners.Contains(value);
			return false;
		}

		int IList.IndexOf(object value)
		{
			if (value is ILogListener)
				return listeners.IndexOf(value);
			return -1;
		}

		void IList.Insert(int index, object value)
		{
			if (value is ILogListener)
			{
				Insert(index, (ILogListener)value);
				return;
			}
			throw new NotSupportedException("You can only insert ILogListener objects into the collection");
		}

		void IList.Remove(object value)
		{
			if (value is ILogListener)
				listeners.Remove(value);
		}

		public int IndexOf(ILogListener listener)
		{
			return listeners.IndexOf(listener);
		}

		public void Insert(int index, ILogListener listener)
		{
			InitializeListener(listener);
			listeners.Insert(index, listener);
		}

		public void Remove(string name)
		{
			ILogListener found = null;

			lock (listeners.SyncRoot)
			{
				foreach (ILogListener listener in listeners)
				{
					if (listener.Name == name)
					{
						found = listener;
						break;
					}
				}

				if (found != null)
					listeners.Remove(found);
				else
					throw new ArgumentException("ILogListener " + name + " was not in the collection");
			}
		}

		public void Remove(ILogListener listener)
		{
			listeners.Remove(listener);
		}

		public void RemoveAt(int index)
		{
			listeners.RemoveAt(index);
		}



	}
}