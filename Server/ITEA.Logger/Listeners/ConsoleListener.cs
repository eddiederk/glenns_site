﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITEA.Logger.Listeners
{
	public class ConsoleListener : LogListener<ListenerSettings>
	{
		public ConsoleListener() 
		{
			Name = "Console";
		}
		public override void Write(LogData message)
		{
			string s = GetFormattedMessage(message);
			Console.WriteLine(s);
		}

		public override void Flush() { }

		public override MessageType LogMessageType { get { return MessageType.Default; } }
	}
}
