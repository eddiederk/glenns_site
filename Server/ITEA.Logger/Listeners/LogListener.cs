﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITEA.Logger
{
	public class LogData
	{
		public string Message;
		public DateTime DateTime = DateTime.Now;
		public int ThreadId;
		public int ProcessId;
		public string CallStack;
		public MessageType MessageType;
		public object[] Args;
		public string ExceptionMessage;
	}

	namespace Listeners
	{
		public interface ILogListener : IDisposable
		{
			string Name { get; set; }
			//long TraceLevel { get; set; }
			ListenerSettings Settings { get; set; }
			void Flush();
			void UpdateConfig(ListenerSettings settings);
			void Initalize(Logger logger);
			void Write(LogData message);
			bool IsDisposed { get; }
		}

		/// <summary>Provides the abstract base class for the listeners who monitor logging output.</summary>
		/// <typeparam name="T">Listener Settings Class</typeparam>
		/// <typeparam name="U">Auto load custom MessageType Class into MessageType</typeparam>
		public abstract class LogListener<T, U> : LogListener<T> where T : ListenerSettings, new() where U : IMessageType
		{
			public LogListener() : base()
			{
				MessageType.Load<U>();
			}
		}


		/// <summary>Provides the abstract base class for the listeners who monitor logging output.</summary>
		/// <typeparam name="T">Settings class implementing <see cref="ITEA.Logger.Listeners.ListenerSettings"/></typeparam>
		public abstract class LogListener<T> : IDisposable, Listeners.ILogListener where T : ListenerSettings, new()
		{
			#region Constructor / Init

			public LogListener(T settings) 
			{
				if (settings == null) Settings = new T();
				else Settings = settings; 
			}

			public LogListener() 
			{ 
				Settings = new T();
			}
			
			public virtual void Initalize(Logger logger) {  }

			#endregion

			#region Fields
			public bool IsDisposed { get; private set; }

			public string Name { get; set; }
			public int _id;
			#endregion

			#region Properties
			public virtual LogOptions LogOutputOptions { get { return Settings.Options; } set { Settings.Options = value; } }
			public virtual string DateTimeFormat { get { return Settings.DateTimeFormat; } set { Settings.DateTimeFormat = value; } }
			public T Settings { get; set; }
			ListenerSettings ILogListener.Settings
			{
				get { return (ListenerSettings)this.Settings; }
				set { this.Settings = value as T; }
			}
			#endregion

			#region Abstract
			public abstract void Write(LogData message);
			public abstract void Flush();
			public abstract MessageType LogMessageType { get; }
			#endregion

			public virtual string GetFormattedMessage(LogData msg)
			{
				string header = "";
				string footer = "";
				string body = "";

				if (Settings.Options.HasFlag(LogOptions.DateTime) && msg.DateTime > DateTime.MinValue) header += msg.DateTime.ToString(DateTimeFormat) + " ";

				header += "[ " + msg.MessageType.Character;

				if (Settings.Options.HasFlag(LogOptions.ThreadId) && msg.ThreadId > 0) header += " T:" + msg.ThreadId;

				if (Settings.Options.HasFlag(LogOptions.ProcessId) && msg.ProcessId > 0) header += " P:" + msg.ProcessId;

				if (Settings.Options.HasFlag(LogOptions.CallstackError)||Settings.Options.HasFlag(LogOptions.CallstackVerbose) && !string.IsNullOrEmpty(msg.CallStack)) 
					footer = "\r\nCallstack:" + msg.CallStack;

				try
				{
					body = string.Format(msg.Message, msg.Args);
				}
				catch (Exception)
				{
					body = msg.Message;
				}

				return header + "]\t" + body + footer;
			}

			public void LogMessage(string strMessage, params object[] args) { Log.Write(LogMessageType,strMessage,args); }
			public void LogMessage(string strMessage, Exception ex, params object[] args) { Log.Write(LogMessageType, ex, strMessage, args); }

			public virtual void UpdateConfig(ListenerSettings settings)
			{
				if (settings is T == false) throw new NotSupportedException("Expected Type: " + typeof(T).Name);
				this.Settings = settings as T;
			}

			protected virtual void Dispose(bool disposing)
			{
				if (!this.IsDisposed)
				{
					this.Flush();
					if (disposing)
					{	// Release Managed Resources.

					}
					// Release unmanaged resources.				
				}
				IsDisposed = true;
			}
			public virtual void Dispose()
			{
				Dispose(true);
				GC.SuppressFinalize(this);
			}
		}

	}
	
}
