﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Threading;
using System.Reflection;

namespace ITEA.Logger.Listeners
{
	public class DBListener : LogListener<DBListenerSettings, DBMessageType>
	{
		private System.Text.StringBuilder sbLogMessages = new StringBuilder(50000, 250000);
		private IsWriting isWriting = new IsWriting();
		private string SQLInsert = "INSERT INTO {0} ([Application], [Server], MessageType, [DateTime], ThreadID, ProcessId, [Message], StackTrace) VALUES \r\n";
		private string SQLFormat = "('{0}', '{1}','{2}','{3}',{4},{5},'{6}','{7}'),";
		private bool _Enabled = false;
		
		public DBListener()
		{
			this.Settings = new DBListenerSettings();
			Settings.TraceLevel = DBMessageType.Error | DBMessageType.Warning | DBMessageType.MinorError;
		}

		public override void Initalize(Logger logger)
		{
			base.Initalize(logger);
			try
			{
				if (string.IsNullOrWhiteSpace(Settings.AppName))
				{
					try
					{
						var assembly = Assembly.GetEntryAssembly();
						AssemblyTitleAttribute title =Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false).First() as AssemblyTitleAttribute;
						Settings.AppName = title.Title;
					}
					catch (Exception ex)
					{
						Settings.AppName = AppDomain.CurrentDomain.FriendlyName;
					}
				}
				
				if (string.IsNullOrWhiteSpace(Settings.ServerName)) Settings.ServerName = Environment.MachineName;
			}
			catch (Exception ex)
			{
				LogMessage("Failed to Update ServerName or AppName", ex);
			}

			if (string.IsNullOrWhiteSpace(Settings.ConnectionString))
			{
				LogMessage("DB Logger is DISABLED because ConnectionString is Missing");
				return;
			}

			_Enabled = true;
		}

		private string ScrubText(string text)
		{
			if (string.IsNullOrWhiteSpace(text)) return "NULL";

			string str = text.Replace("'", "''");//Fix issues with singlequotes
			str = str.Replace("\r\n", "' + CHAR(13)+CHAR(10) + '");//Fix Line Feed
			str = str.Replace("\t", "' + CHAR(9) + '");//Fix Tab
			return str;
		}

		public override void Write(LogData message)
		{
			if (!_Enabled) return;
			if (!Generics.HasABitMatch(Settings.TraceLevel, message.MessageType)) return;
			string s = string.Format(SQLFormat,
				Settings.AppName,
				Settings.ServerName,
				message.MessageType.Name,
				message.DateTime.ToString(),
				message.ThreadId,
				message.ProcessId,
				ScrubText(message.Message),
				ScrubText(message.CallStack)
			);

			lock (sbLogMessages)
			{
				sbLogMessages.AppendLine(s);
			}

			if (sbLogMessages.Length > 500) Flush();
		}

		public override void Flush()
		{
			bool shouldWriteLogFile;

			lock (isWriting)
			{
				shouldWriteLogFile = ShouldWrite();
			}

			if (shouldWriteLogFile)
			{
				LogMessage("Beginning to write to DB Log.");
				string sql = string.Empty;
				lock (sbLogMessages)
				{
					sql = sbLogMessages.ToString().TrimEnd(',');
					sbLogMessages.Length = 0;
				}
				System.Threading.Thread threadLogMessage = new Thread(new System.Threading.ThreadStart(() => { RetrySQL(WriteToDB, sql); }));
				threadLogMessage.Start();
			}
		}

		private bool ShouldWrite()
		{
			if (!isWriting.Currently && sbLogMessages.Length > 0)
			{
				isWriting.Currently = true;
				return true;
			}
			else return false;
		}

		private void WriteToDB(string sql)
		{
			using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
			{
				cn.Open();
				SqlCommand cmd = new SqlCommand(String.Format(SQLInsert, Settings.TableName) + sql, cn);
				int cnt = cmd.ExecuteNonQuery();

				LogMessage("{0} rows added to {1}",cnt, Settings.TableName);
			}
		}

		private class IsWriting
		{
			private bool isWriting = false;

			public bool Currently
			{
				get { return isWriting; }
				set { isWriting = value; }
			}

		}

		public override MessageType LogMessageType { get { return DBMessageType.DBLog; } }
		
		#region DBRetry
		private const double LONG_WAIT_SECONDS = 5;
		private const double SHORT_WAIT_SECONDS = 0.5;
		private const int MAX_RETRY = 3;
		private static readonly TimeSpan longWait = TimeSpan.FromSeconds(LONG_WAIT_SECONDS);
		private static readonly TimeSpan shortWait = TimeSpan.FromSeconds(SHORT_WAIT_SECONDS);
		private enum RetryableSqlErrors
		{
			Timeout = -2,
			NoLock = 1204,
			Deadlock = 1205,
			WordbreakerTimeout = 30053,
		}

		private void RetrySQL(Action<string> retryAction, string sql)
		{
			var retryCount = 0;
			try
			{
				for (; ; )
				{
					try
					{
						retryAction(sql);
						break;
					}
					catch (SqlException ex)
					{
						if (!Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number))
							throw;

						retryCount++;
						if (retryCount > MAX_RETRY) throw;

						Thread.Sleep(ex.Number == (int)RetryableSqlErrors.Timeout ?
																			longWait : shortWait);
					}
				}
			}
			catch (Exception ex)
			{
				LogMessage("Error! Logs will be lost.  Failed to write to Logging DB Table:" + Settings.TableName, ex);
			}

			isWriting.Currently = false;
		}
		#endregion
	}

	public class DBMessageType : MessageType
	{
		internal static MessageType DBLog = MessageType.Add(0x2000000, "dbLog", "DBLog");
	}

	public class DBListenerSettings : ListenerSettings
	{
		public DBListenerSettings()
		{
			ConnectionString = "";
			TableName = "TraceLogs";
		}

		public string AppName { get; set; }
		public string ServerName { get; set; }
		public string ConnectionString { get; set; }
		public string TableName { get; set; }
	}
}
