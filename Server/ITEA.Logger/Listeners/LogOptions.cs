﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITEA.Logger
{
	[Flags]
	[TypeConverter(typeof(Config.EnumToStringConverter))]
	public enum LogOptions
	{
		/// <summary>Do not write any elements.</summary>
		None = 0,

		///// <summary>Write the logical operation stack, which is represented by the return value of the System.Diagnostics.CorrelationManager.LogicalOperationStack property.</summary>
		//LogicalOperationStack = 1,

		/// <summary>Write the date and time.</summary>
		DateTime = 2,

		/// <summary>Write the timestamp, which is represented by the return value of the System.Diagnostics.Stopwatch.GetTimestamp()method.</summary>
		//Time = 4,

		/// <summary> Write the process identity, which is represented by the return value of the System.Diagnostics.Process.Id property.</summary>
		ProcessId = 8,

		/// <summary> Write the thread identity, which is represented by the return value of the System.Threading.Thread.ManagedThreadId property for the current thread.</summary>
		ThreadId = 16,

		/// <summary>Write the call stack, which is represented by the return value of the System.Environment.StackTrace property.</summary>
		CallstackError = 32,

		/// <summary>Write the call stack, which is represented by the return value of the System.Environment.StackTrace property.</summary>
		CallstackVerbose = 64,
	}
}
