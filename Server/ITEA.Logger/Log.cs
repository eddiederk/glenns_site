﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITEA.Logger.Listeners;
namespace ITEA.Logger
{
	/// <summary>
	/// Static implementation of the ConsoleLogger methods
	/// </summary>
	public class Log
	{
		private static Logger _log;
		internal static Command.CommandManager CommandManager
		{ 
			get 
			{ 
				if(_log == null) throw new ArgumentNullException("Logger has not been initalized yet.");
				if (_log.CommandManager == null) throw new ArgumentNullException("This Logger isn't configured to utilize the command manager.");
				return _log.CommandManager;
			} 
		}
		private static bool IsDisposed = false;
		// Can only be instantiated by ConsoleLogger itself
		internal Log(Logger log)
		{
			_log = log;
		}
		
		public static void Dispose()
		{
			IsDisposed = true;
			if (_log != null)
				_log.Dispose();

			_log = null;
		}

		#region Properties
		public static bool IsVerboseSet { get { return _log == null ? false : _log.IsVerboseSet; } }
		public static bool IsDebuggingSet { get { return _log == null ? false : _log.IsDebuggingSet; } }
		public long EnabledFlags { get { return _log == null ? 0 : _log.EnabledFlags; } }

		public bool MessageTypeIsEnabled(MessageType messageType) { return _log == null ? false : _log.MessageTypeIsEnabled(messageType); }
		// public static Logger Logger { get { return _log; } }
		#endregion

		#region LogMessage
		public static void LogMessage(string strMessage)
		{
			if (IsDisposed) return;
			if (_log != null)
				_log.LogMessage(strMessage);
			else
				throw new Exception("ConsoleLogger has not been instantiated.");
		}
		public static void LogMessage(string strMessage, MessageType messageType)
		{
			if (_log == null || (_log.EnabledFlags & messageType) != messageType) return; //Quick Short Circuit
			_log.LogMessage(strMessage, messageType);
			//            else
			//                throw new Exception("ConsoleLogger has not been instantiated.");
		}
		public static void LogMessage(string strMessage, MessageType messageType, bool ForceFileWrite)
		{
			if (_log == null || (_log.EnabledFlags & messageType) != messageType) return; //Quick Short Circuit
			_log.LogMessage(strMessage, messageType, ForceFileWrite);
			//else
			//throw new Exception("ConsoleLogger has not been instantiated.");
		}
		public static void LogMessage(string strMessage, MessageType messageType, Exception ex)
		{
			if (_log == null || (_log.EnabledFlags & messageType) != messageType) return; //Quick Short Circuit
			_log.LogMessage(strMessage, messageType, ex);
			//else
			//throw new Exception("ConsoleLogger has not been instantiated.");
		}
		#endregion

		#region Write

		public static void FlushLogs() 
		{	// Only flush if _log exists.
			if (_log != null) _log.Flush();
		}

		public static void Write(MessageType messageType, string strMessage, params object[] args)
		{
			if (IsDisposed) return;
			if (_log == null) Logger.Create();
			_log.LogMessage(messageType, strMessage, args);
		}

		public static void Write(MessageType messageType, Exception ex, string strMessage, params object[] args)
		{
			if (IsDisposed) return;
			if (_log == null) Logger.Create();
			_log.LogMessage(messageType, ex,strMessage, args);
		}

		public static void WriteTo<T>(MessageType messageType, string strMessage, params object[] args) where T : ILogListener
		{
			if (IsDisposed) return;
			if (_log == null) Logger.Create();
			_log.LogMessageTo<T>(messageType, strMessage, null, args); 
		}

		public static void WriteTo<T>(MessageType messageType, Exception ex, string strMessage, params object[] args) where T : ILogListener
		{
			if (IsDisposed) return;
			if (_log == null) Logger.Create();
			_log.LogMessageTo<T>(messageType, strMessage, ex, args);
		}

		internal static void WriteMessageArgs<T>(string source, MessageType messageType, Exception ex, string strMessage, params object[] args)
		{
			if (IsDisposed) return;
			if (_log == null) Logger.Create();
			_log.WriteMessageArgs<T>(source, messageType, ex, strMessage, args);
		}

		#endregion

		public static string GetExceptionMessage(Exception ex, bool showStackTrace=false)
		{
			return Logger.GetExceptionMessage(ex, showStackTrace);
		}

		public static void SaveConfig() { _log.SaveConfig(); }

	}
}
