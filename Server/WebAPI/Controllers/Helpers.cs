﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Server.Controllers
{
	public static class Extensions
	{
		internal static void ThrowIfHasValidationErrors(this DbEntities db, HttpRequestMessage request)
		{
			var validationErrors = db.GetValidationErrors();
			if (validationErrors != null && validationErrors.Count() > 0)
			{
				var ModelState = new ModelStateDictionary();
				foreach (var v in validationErrors)
				{
					foreach (var e in v.ValidationErrors)
					{
						ModelState.AddModelError(e.PropertyName, e.ErrorMessage);
					}
				}

				throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState));
			}
		}
	}
}
