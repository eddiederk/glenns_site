﻿using ITEA.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Microsoft.AspNet.Identity;

namespace Server.Controllers
{
    public class BaseDBController : ApiController
    {
		protected internal DbEntities db = new DbEntities();
		protected string UserID;
		protected string lid = "";
		
		protected override void Initialize(System.Web.Http.Controllers.HttpControllerContext controllerContext)
		{
			base.Initialize(controllerContext);
			
			this.lid = this.Request.GetLogID();
			this.UserID = ((ClaimsIdentity)User.Identity).GetUserId();
		}
    }

}
