﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Server.Controllers
{
	[RoutePrefix("livetest")]
	public class LiveTestController : BaseDBController
    {
		[Route("ping")]
		public IHttpActionResult GetPing()
		{
			return Ok("{ 'Question':'How are you?', 'answer':'AMAZING!!'}");
		}
    }
}
