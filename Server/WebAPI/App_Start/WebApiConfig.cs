﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Tracing;
using ITEA.Logger;
namespace Server
{
    public static class WebApiConfig
    {
		public static void Register(HttpConfiguration config)
		{
			
			 var w = Stopwatch.StartNew();
			 try
			 {
				 // Web API configuration and services
				 Log.Write(MessageType.Status, "Configuring WebAPI.");

				 //Enable Logging
				 // config.EnableSystemDiagnosticsTracing();
				 // SystemDiagnosticsTraceWriter traceWriter = config.EnableSystemDiagnosticsTracing();
				 // traceWriter.MinimumLevel = System.Web.Http.Tracing.TraceLevel.Debug;
				 //System.Diagnostics.Trace.Listeners.Add(new tl());
				 config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

				 config.MessageHandlers.Add(new ScrubErrorsHandler());
				 config.MessageHandlers.Add(new LogHandler());
				 

				 // Web API routes
				 config.MapHttpAttributeRoutes();

				 config.Routes.MapHttpRoute(
					  name: "DefaultApi",
					  routeTemplate: "{controller}/{id}",
					  defaults: new { id = RouteParameter.Optional }
				 );

				 //Don't let browser cache the WEB API Data until we figure out caching
				 // Solved via build process: pending validation
				 // config.Filters.Add(new NoCacheHeaderFilter());
				 

				 var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
				 jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				 jsonFormatter.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;

				 Log.Write(MyMessageType.Performance, "Configuring WebAPI took {0} m/s.", w.ElapsedMilliseconds);
			 }
			 catch (Exception ex)
			 {
				 Log.Write(MessageType.Error, ex, "Failed to Configure WebAPI");
			 }
		 }
    }

	 public class NoCacheHeaderFilter : ActionFilterAttribute
	 {
		 public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
		 {
			 try
			 { 
				 if (actionExecutedContext == null || actionExecutedContext.Response == null) return;
				 if (actionExecutedContext.Response.StatusCode == System.Net.HttpStatusCode.NoContent) return;

				 actionExecutedContext.Response.Headers.CacheControl =  new CacheControlHeaderValue()
				 {
					 NoCache = true,
					 NoStore = true,
					 MaxAge = new TimeSpan(0),
					 MustRevalidate = true
				 };

				 actionExecutedContext.Response.Headers.Pragma.Add(new NameValueHeaderValue("no-cache"));
				 if (actionExecutedContext.Response.Content != null)
					actionExecutedContext.Response.Content.Headers.Expires = new DateTime(1990, 1, 1);
			 }
			 catch (Exception ex)
			 {
				 Log.Write(MessageType.Error, ex, "Failed to Add no cached to header.");
			 }
		 }
	 }
}
