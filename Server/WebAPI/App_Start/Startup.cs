﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using ITEA.Logger;
using System.Diagnostics;
using System.Web.Http.Tracing;

//[assembly: OwinStartup(typeof(Server.Startup))]

namespace Server
{
	public class Startup
	{

		public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }

		public void Configuration(IAppBuilder app)
		{
			try
			{
				Log.Write(MessageType.Status, "Configuring OAuth. ");

				ConfigureOAuthTokenConsumption(app);

				// GlobalConfiguration.Configure(WebApiConfig.Register);

				HttpConfiguration config = new HttpConfiguration();


				//WebApiConfig.Register(config);
				app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
				app.UseWebApi(config);

				Log.Write(MessageType.Status, "OAuth Configured.");
				
			}
			catch (Exception ex) {
				Log.Write(MessageType.Error, ex, "Failed to Configure OAuth");
			}
			Log.FlushLogs();
		}

		private void ConfigureOAuthTokenConsumption(IAppBuilder app)
		{
			try
			{
				var issuer = ConfigurationManager.AppSettings["as:AuthIssuer"];

				string clientId = ConfigurationManager.AppSettings["as:ClientId"];
				byte[] clientSecret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["as:ClientSecret"]);

				// Api controllers with an [Authorize] attribute will be validated with JWT
				app.UseJwtBearerAuthentication(
					 new JwtBearerAuthenticationOptions
					 {
						 AuthenticationMode = AuthenticationMode.Active,
						 AllowedAudiences = new[] { clientId },
						 IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
						  {
								new SymmetricKeyIssuerSecurityTokenProvider(issuer, clientSecret)
						  },
						 //Provider = new OAuthBearerAuthenticationProvider
						 //{
						 //	OnValidateIdentity = context =>
						 //	{
						 //		//TODO: Set Application Level Claims & Roles
						 //	  // context.Ticket.Identity.AddClaim(new System.Security.Claims.Claim("newCustomClaim", "newValue"));
						 //		return Task.FromResult<object>(null);
						 //	}
						 //}
					 });
			}
			catch (Exception ex)
			{
				Log.Write(MessageType.Error, ex, "ConfigureOAuthTokenConsumption Failed");
			}
		}	


	}


	public class tl : TraceListener, ILogSource
	{

		public override void Write(string message) { }

		public override void WriteLine(string message)
		{
			if (message.StartsWith(DateTime.Today.ToString("MM/dd/yyyy"))) return;
			if (message.StartsWith("Request")) this.LogMessage(MyMessageType.WebAPIRequest, message);
			else if (message.StartsWith("Respose")) this.LogMessage(MyMessageType.WebAPIResponse, message);
			else if(Log.IsVerboseSet) this.LogMessage(MessageType.Verbose, message);
		}

		public string SourceName { get { return "HTTP"; } }
	}
}
