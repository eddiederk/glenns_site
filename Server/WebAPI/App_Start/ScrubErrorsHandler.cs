﻿using ITEA.Logger;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace Server
{
	public class ScrubErrorsHandler : DelegatingHandler
	{
		private const string NOTIFY_MESSAGE = "Our support personal have been notified of the details of this error and will work to resolve it as soon as possible. Please try again later. Thank you for your patience.";
		protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			HttpResponseMessage response = await base.SendAsync(request, cancellationToken);
			HttpError error;
			if (!response.IsSuccessStatusCode && response.TryGetContentValue(out error))
			{
				var lid = request.GetLogID();
				var mt = (int)response.StatusCode >= 500 ? MyMessageType.Error : MyMessageType.id10T;
				Log.Write(mt, "{0}{1}", lid, ExtractLog(error));
				
				// Use an HttpError that doesn't leak internal information
				var newError = CreateSafeResponse(response.StatusCode, error);

				Debug.WriteLine(ExtractLog(error));

				return request.CreateErrorResponse(response.StatusCode, newError);
			}

			return response;
		}
		
		private HttpError CreateSafeResponse(HttpStatusCode statusCode, HttpError error)
		{
			 var t = typeof(HttpError);

			HttpError newError = null;
			newError = new HttpError();

			newError.Message = CreateClientMessage(error);
			if (error.ModelState != null) newError.Add("formErrors", this.TransformModelState(error.ModelState));
			
			// InternalServerError = 500, NotImplemented = 501, BadGateway = 502, ServiceUnavailable = 503, GatewayTimeout = 504, HttpVersionNotSupported = 505
			if ((int)statusCode >= 500)
			{
				if (error.Message == "") ;
				// TODO: Some SQL Save failures should prompt user to try again.
				//		e.g. SQL Deadlock
				newError.MessageDetail = NOTIFY_MESSAGE;
			}
			return newError;
		}

		private static string[] safeExceptions = { "System.ApplicationException" };
		private string CreateClientMessage(HttpError error)
		{
			// Allow specific exceptions to bubble up the Exception Message.
			if (error.ContainsKey("ExceptionType") && error.ContainsKey("ExceptionMessage"))
			{
				var et = error["ExceptionType"].ToString();
				//Custom Exceptions OR safeExceptions can pass message to client.
				if (!et.StartsWith("System") || safeExceptions.Contains(et))
					return error["ExceptionMessage"].ToString();
			}
			
			// We don't care about generic error message.
			if (error.Message != "An error has occurred.") return error.Message;
			return null;
		}

		private string ExtractLog(HttpError error)
		{
			string stackTrace;
			string result = ExtractLog(error, out stackTrace);
			if (!string.IsNullOrEmpty(stackTrace))
				result = result + "---------------STACK TRACE---------------\r\n" + stackTrace + "\r\n---------------END OF STACK TRACE---------------\r\n";
			return result.Replace('{', '[').Replace('}', ']');
		}

		private string ExtractLog(HttpError error, out string stackTrace)
		{
			stackTrace = "";
			string st = "";
			StringBuilder sb = new StringBuilder();
			foreach (var e in error)
			{
				if (e.Value is HttpError)
				{
					sb.AppendLine(ExtractLog((HttpError)e.Value, out stackTrace));
					st += stackTrace;
				}
				else if (e.Key == "StackTrace") st += e.Value.ToString();
				else if (e.Key == "ExceptionType" || e.Value.ToString() == "An error has occurred.") ; //Ignore this field
				else if (e.Key == "ExceptionMessage") sb.AppendFormat("{0}:{1}\r\n", error["ExceptionType"].ToString(), e.Value.ToString());
				else if (e.Value is string[]) sb.AppendFormat("\t{0}=[({1})]\r\n", e.Key, string.Join("),(", (string[])e.Value));
				else sb.AppendFormat("{0}:{1}\r\n", e.Key, e.Value.ToString());
			}
			stackTrace = st;
			return sb.ToString();
		}

		private HttpError TransformModelState(HttpError modelState)
		{
			foreach (var i in modelState.ToArray())
			{
				modelState.Remove(i.Key);
				modelState.Add(i.Key.Split('.').Last(), i.Value);
			}

			return modelState;
		}
	}
}