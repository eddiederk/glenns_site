﻿using ITEA.Logger;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Server
{
	public class LogHandler : DelegatingHandler
	{
		private const string requestFormat = "{0}Method:{1}|Uri:{2}|RequestBody:{3}";
		private const string requestGetFormat = "{0}Method:{1}|Uri:{2}";
		private const string responseFormat = "{0}Duration:{1}|Status:{2}";

		protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			Stopwatch w = Stopwatch.StartNew();
			string content = string.Empty;
			try
			{
				if (Log.IsDebuggingSet && request.Content.Headers.ContentLength > 0 && request.Content.Headers.ContentLength < 400)
				{
					content = await request.Content.ReadAsStringAsync();
				}
			}
			catch (Exception) { } // Ignore failures
			
			var response = await base.SendAsync(request, cancellationToken);
			string id = request.GetLogID();
			
			if(!content.ToLower().Contains("password") && !content.ToLower().Contains("accountnumber"))
				Log.Write(MyMessageType.WebAPIRequest, requestFormat, id, request.Method, request.RequestUri.OriginalString, content);
			
			Log.Write(MyMessageType.WebAPIResponse, responseFormat, id, w.ElapsedMilliseconds, response.StatusCode, response.ReasonPhrase);
			return response;
		}
	}

	public static class HttpRequestMessageExtension
	{
		public static string GetLogID(this HttpRequestMessage m)
		{
			string correlationId = "";
			string userName = "";
			string sessionId = "";
			var identity = m.GetRequestContext().Principal.Identity as ClaimsIdentity;

			if (m != null) correlationId = m.GetCorrelationId().ToString().Substring(0,12);
			if(identity != null) {
				userName = identity.Name;
				var claim = identity.FindFirst("SessionID");
				if(claim != null) sessionId = claim.Value;
			}
			
			return string.Format("<{0}>|<{1}>|<{2}>",  sessionId, userName, correlationId);
		}
	}

}