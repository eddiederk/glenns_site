﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using ITEA.Logger;
using System.Web.Http.Results;
using Server;

namespace AspNetIdentity.WebApi.Infrastructure
{
	public class ClaimsAuthorizationAttribute : AuthorizationFilterAttribute
	{
		public string ClaimType { get; set; }
		public string ClaimValue { get; set; }
		public string URIParam { get; set; }
		public string QueryParam { get; set; }

		public override Task OnAuthorizationAsync(HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken)
		{
			string id = "";// actionContext.Request.GetLogID();
			try
			{
				
				string value = ClaimValue;
				var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

				if (principal.IsInRole("SuperAdmin")) return Task.FromResult<object>(null);

				if (!principal.Identity.IsAuthenticated)
				{
					Log.Write(MessageType.Warning, "{0}IsAuthenticated = false;",id);
					actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
				}
				else
				{
					if (!string.IsNullOrWhiteSpace(URIParam))
					{
						
						//Will get you the resource id assuming a default route like /api/foo/{id} 
						var routeData = actionContext.Request.GetRouteData();
						
						//Sometimes the parameter is not passed and should not be authorized.
						//this allows us to put the Attribute at the class level
						//instead of on PUT, POST, DELETE and others
						if (routeData.Values.ContainsKey(URIParam))
							value = routeData.Values[URIParam] as string;
						else return Task.FromResult<object>(null);

					}

					if (!string.IsNullOrWhiteSpace(QueryParam))
					{
						//uri is still accessible so use this to get query params
						var queryString = HttpUtility.ParseQueryString(actionContext.Request.RequestUri.Query);
						value = queryString[QueryParam];
					}


					var response = ValidateClaim(ClaimType, value, principal);
					if (response != HttpStatusCode.OK)
						actionContext.Response = actionContext.Request.CreateResponse(response);
				}
			}
			catch (Exception ex)
			{
				Log.Write(MessageType.Error, ex, "{0}OnAuthorizationAsync Failed.",id);
			}
			
			//complete execution
			return Task.FromResult<object>(null);
		}

		internal static HttpStatusCode ValidateClaim(string claimType, string value, ClaimsPrincipal principal)
		{
			try
			{
				var claims = principal.FindAll(claimType);
				if (claims.Count() == 0) return HttpStatusCode.Forbidden;
				else if (!string.IsNullOrEmpty(value) && !claims.Any(x => x.Value == value)) return HttpStatusCode.Forbidden;
				else return HttpStatusCode.OK;
			}
			catch (Exception ex)
			{
				Log.Write(MessageType.Error, ex, "Failed to Validate User Claims");
				return HttpStatusCode.Forbidden;
			}
		}
	}

	public class Authorize403Attribute : AuthorizeAttribute
	{
		protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
		{
			string id = "";// actionContext.Request.GetLogID();
			try
			{
				if (actionContext.RequestContext.Principal.Identity.IsAuthenticated)
				{
					Log.Write(MessageType.Warning, "{0}IsAuthenticated=false", id);
					actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
					actionContext.Response.ReasonPhrase = "You're not supposed to be here... Slowly back away, and we will pretend this never happened.";
					actionContext.Response.RequestMessage = actionContext.Request;
				}
				else
				{
					Log.Write(MessageType.Warning, "{0}Unauthorized User",id);
					actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
					actionContext.Response.ReasonPhrase = "You gotta login first!";
					actionContext.Response.RequestMessage = actionContext.Request;
					//base.HandleUnauthorizedRequest(actionContext);
				}
			}
			catch (Exception ex)
			{
				Log.Write(MessageType.Error, ex, "{0}HandleUnauthorizedRequest Failed",id);
			}
		}
	}
}