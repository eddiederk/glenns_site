﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITEA.Logger;

namespace Server
{
	public class MyMessageType : MessageType
	{
		public static readonly MyMessageType Performance =			new MyMessageType(0x00002000, "PRF", "Performance");
		public static readonly MyMessageType WebAPIRequest =		new MyMessageType(0x00004000, "REQ", "Request");
		public static readonly MyMessageType WebAPIResponse =		new MyMessageType(0x00008000, "RES", "Respose");
		public static readonly MyMessageType id10T =				new MyMessageType(0x00020000, "id10T", "id10T");

		// TODO: Add additional message types here.

		private MyMessageType(long value, string character, string name) : base(value, character, name) { }
	}
}
