﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using ITEA.Logger;
using System.Web.Http;
using System.Configuration;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using System.Net;

namespace Server
{
	public class Global : System.Web.HttpApplication
	{

		protected void Application_Start(object sender, EventArgs e)
		{
			//For Vault
			System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

			HttpConfiguration config = GlobalConfiguration.Configuration;
			config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
			
			string strMyPath = AppDomain.CurrentDomain.BaseDirectory;
			string strMyName = AppDomain.CurrentDomain.BaseDirectory.Split(new char[]{'\\'}, StringSplitOptions.RemoveEmptyEntries).Last();

			try
			{
				var l = Logger.Create<MyMessageType>(strMyName, strMyPath);
				l.LogMessage(MessageType.Status, "Application Start Called");
				l.LogMessage(MessageType.Status, "PipeName:{0}", strMyName);


				GlobalConfiguration.Configure(WebApiConfig.Register);
				
				l.LogMessage(MessageType.Status, "Application Startup Completed.");
			}
			catch (Exception ex)
			{
				Log.Write(MessageType.Error, ex, "Failed to Configure App Start");
			}

			Log.FlushLogs();
		}

		protected void Session_Start(object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{
			// System.Threading.Thread.Sleep(5000);
		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{

		}

		protected void Application_Error(object sender, EventArgs e)
		{

		}

		protected void Session_End(object sender, EventArgs e)
		{

		}

		protected void Application_End(object sender, EventArgs e)
		{
			Log.Write(MessageType.Status, "Ending Application.");
			Log.Dispose();
		}	
	}
}